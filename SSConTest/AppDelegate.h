//
//  AppDelegate.h
//  SSConTest
//
//  Created by mint on 13. 10. 4..
//  Copyright (c) 2013년 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate, NSStreamDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
