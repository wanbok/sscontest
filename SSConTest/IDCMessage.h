//
//  IDCMessage.h
//  SSConTest
//
//  Created by Yoon Sungwook on 10/11/13.
//
//

#import <Foundation/Foundation.h>

@interface IDCMessage : NSObject

/**
 @desc length of parameter. set automatically when calling -[self data:]
 */
@property (assign) int parameterLength;

/**
 @desc sequence number. should be set by IDCCommunicator
 */
@property (assign) int sequenceNumber;

/**
 @desc main command. set automatically when calling -[self data:]
 */
@property (retain) NSString *mainCommand;

/**
 @desc sub command. set automatically when calling -[self data:]
 */
@property (retain) NSString *subCommand;

/**
 @desc command type. calling -[self data:] sets this to "REQUEST"
 */
@property (retain) NSString *commandType;

/**
 @desc parameter. should be set by subclasses
 */
@property (retain) NSString *parameter;

/**
 @desc construct data complying to IDC message format with the configured properties
 */
- (NSData *)data;

/**
 @desc set properties with data. data should be compliant with IDC message format.
 */
- (id)initWithData:(NSData *)data;

/**
 @desc Generate an IDCMessage or its subclass appropritate to the given data and set properties with data. data should be compliant with IDC message format.
 */
+ (id)idcMessageWithData:(NSData *)data;

/**
 @desc mainCommand represented by the class. subclasses should implement the method
 */
+ (NSString *)mainCommand;

/**
 @desc subCommand represented by the class. subclasses should implement the method
 */
+ (NSString *)subCommand;

/**
 @desc string representing the message.
 */
- (NSString *)stringMessage;

@end
