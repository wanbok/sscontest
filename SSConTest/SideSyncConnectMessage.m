//
//  SideSyncConnectMessage.m
//  SSConTest
//
//  Created by Yoon Sungwook on 10/10/13.
//
//

#import "SideSyncConnectMessage.h"

NSString *SideSyncConnectTypePC = @"PC";
NSString *SideSyncConnectTypeTablet = @"TABLET";
NSString *SideSyncConnectTypeMobile = @"MOBILE";

NSString *SideSyncConnectNetWireless = @"WIRELESS";
NSString *SideSyncConnectNetUSB = @"USB";


@implementation SideSyncConnectMessage

@synthesize serverPort = _serverPort;
@synthesize resolution = _resolution;
@synthesize bluetoothMacAddress = _bluetoothMacAddress;
@synthesize wifiMacAddress = _wifiMacAddress;
@synthesize shouldTryBluetoothCall = _shouldTryBluetoothCall;
@synthesize type = _type;
@synthesize net = _net;
@synthesize name = _name;

+ (NSString *)mainCommand
{
    return @"CONNECT";
}

+ (NSString *)subCommand
{
    return @"SIDESYNC_CONN";
}

- (NSData *)data
{
    NSMutableArray *parameters = [NSMutableArray new];
    [parameters addObject:[NSString stringWithFormat:@"%d", self.serverPort]];
    [parameters addObject:self.bluetoothMacAddress ? self.bluetoothMacAddress : @""];
    [parameters addObject:self.wifiMacAddress ? self.bluetoothMacAddress : @""];
    [parameters addObject:self.shouldTryBluetoothCall ? @"true" : @"false"];
    [parameters addObject:[NSString stringWithFormat:@"%dx%d", (int)self.resolution.width, (int)self.resolution.height]];
    [parameters addObject:self.type];
    [parameters addObject:self.net];
    [parameters addObject:self.name];
    self.parameter = [parameters componentsJoinedByString:@"/&%"];
    
    return [super data];
}

- (id)initWithData:(NSData *)data
{
    self = [super initWithData:data];
    if (self) {
        NSArray *parameters = [self.parameter componentsSeparatedByString:@"/&%"];
        self.serverPort = [[parameters objectAtIndex:0] intValue];
        self.bluetoothMacAddress = [parameters objectAtIndex:1];
        self.wifiMacAddress = [parameters objectAtIndex:2];
        self.shouldTryBluetoothCall = [[parameters objectAtIndex:3] boolValue];
        NSString *resolutionString = [parameters objectAtIndex:4];
        NSArray *resolutionParts =[resolutionString componentsSeparatedByString:@"x"];
        CGFloat width = [[resolutionParts objectAtIndex:0] floatValue];
        CGFloat height = [[resolutionParts objectAtIndex:1] floatValue];
        self.resolution = CGSizeMake(width, height);
        self.type = [parameters objectAtIndex:5];
        self.net = [parameters objectAtIndex:6];
        self.name = [parameters objectAtIndex:7];
    }
    return self;
}

@end
