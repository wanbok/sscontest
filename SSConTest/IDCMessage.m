//
//  IDCMessage.m
//  SSConTest
//
//  Created by Yoon Sungwook on 10/11/13.
//
//

#import "IDCMessage.h"

@implementation IDCMessage

@synthesize parameterLength = _parameterLength;
@synthesize sequenceNumber = _sequenceNumber;
@synthesize mainCommand = _mainCommand;
@synthesize subCommand = _subCommand;
@synthesize commandType = _commandType;
@synthesize parameter = _parameter;

- (NSData *)data
{
    self.mainCommand = [[self class] mainCommand];
    self.subCommand = [[self class] subCommand];
    self.commandType = @"REQUEST";
    self.parameterLength = self.parameter.length;
    NSString *string = [NSString stringWithFormat:@"%02d %d %@ %@ %@ %@", self.parameterLength, self.sequenceNumber, self.mainCommand, self.subCommand, self.commandType, self.parameter];
    return [string dataUsingEncoding:NSUTF8StringEncoding];
}

- (id)initWithData:(NSData *)data
{
    self = [super init];
    if (self) {
        NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSArray *array = [string componentsSeparatedByString:@" "];
        self.parameterLength = [[array objectAtIndex:0] intValue];
        self.sequenceNumber = [[array objectAtIndex:1] intValue];
        self.mainCommand = [array objectAtIndex:2];
        self.subCommand = [array objectAtIndex:3];
        self.commandType = [array objectAtIndex:4];
        self.parameter = [array objectAtIndex:5];
    }
    return self;
}

NSMutableArray *subclasses;
+ (void)initialize
{
    subclasses = [[NSMutableArray alloc] init];
    [subclasses addObject:@"SideSyncConnectMessage"];
    [subclasses addObject:@"AliveCheckMessage"];
}

+ (id)idcMessageWithData:(NSData *)data
{
    NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSArray *array = [string componentsSeparatedByString:@" "];
    if (array.count < 6)
        [NSException raise:@"InvalidIDCMessage" format:@"This message does not seem to be an IDC message."];
    NSString *mainCommand = [array objectAtIndex:2];
    NSString *subCommand = [array objectAtIndex:3];
    
    for (NSString *className in subclasses) {
        Class class = NSClassFromString(className);
        if ([[class mainCommand] isEqualToString:mainCommand] &&
            [[class subCommand] isEqualToString:subCommand]) {
            IDCMessage *message = [[[class alloc] initWithData:data] autorelease];
            return message;
        }
    }
    return [[[self alloc] initWithData:data] autorelease];
}

+ (NSString *)mainCommand
{
    return nil;
}

+ (NSString *)subCommand
{
    return nil;
}

- (NSString *)stringMessage
{
    NSString *string = [NSString stringWithFormat:@"%02d %d %@ %@ %@ %@", self.parameterLength, self.sequenceNumber, self.mainCommand, self.subCommand, self.commandType, self.parameter];
    return string;
}

@end
