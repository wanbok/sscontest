//
//  PSSTester.h
//  SSConTest
//
//  Created by Yoon Sungwook on 10/10/13.
//
//

#import <Foundation/Foundation.h>

@interface PSSTester : NSObject

- (void)start;

@end
