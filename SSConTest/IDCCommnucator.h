//
//  IDCCommnucator.h
//  SSConTest
//
//  Created by Yoon Sungwook on 10/11/13.
//
//

#import <Foundation/Foundation.h>
#import "AsyncSocket.h"
#import "IDCMessage.h"

@class IDCCommnucator;

@protocol IDCCommunicatorDelegate <NSObject>
@optional
- (void)communicatorDidConnect:(IDCCommnucator *)sender;
- (void)communicator:(IDCCommnucator *)sender didReceiveMessage:(IDCMessage *)message;
@end

@interface IDCCommnucator : NSObject

@property (readonly) NSUInteger sequenceNumber;
@property (assign) id<IDCCommunicatorDelegate> delegate;

- (id)initWithHost:(NSString *)hostName port:(int)port;
- (void)start;
- (void)stop;
- (id)initWithSocket:(AsyncSocket *)socket;
- (void)postMessage:(IDCMessage *)message;

@end

