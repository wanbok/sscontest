//
//  SideSyncConnectMessage.h
//  SSConTest
//
//  Created by Yoon Sungwook on 10/10/13.
//
//

#import "IDCMessage.h"

@interface SideSyncConnectMessage : IDCMessage

@property (assign) int serverPort;

@property (assign) NSSize resolution;

@property (retain) NSString *bluetoothMacAddress;

@property (retain) NSString *wifiMacAddress;

@property (assign) BOOL shouldTryBluetoothCall;

@property (retain) NSString *type;
extern NSString *SideSyncConnectTypePC;
extern NSString *SideSyncConnectTypeTablet;
extern NSString *SideSyncConnectTypeMobile;

@property (retain) NSString *net;
extern NSString *SideSyncConnectNetWireless;
extern NSString *SideSyncConnectNetUSB;

@property (retain) NSString *name;

@end
