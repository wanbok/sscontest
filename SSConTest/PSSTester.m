//
//  PSSTester.m
//  SSConTest
//
//  Created by Yoon Sungwook on 10/10/13.
//
//

#import "PSSTester.h"
#import "SideSyncConnectMessage.h"
#import "AliveCheckMessage.h"
#import "IDCCommnucator.h"
#import "AsyncSocket.h"

#define SERVER_PORT 38001

@interface PSSTester () <IDCCommunicatorDelegate, AsyncSocketDelegate>

@end


@implementation PSSTester
{
    AsyncSocket *_serverListener;
    IDCCommnucator *_serverCommunicator;
    IDCCommnucator *_handshaker;
    IDCCommnucator *_mainCommunicator;
    IDCCommnucator *_aliveCheckCommunicator;
    NSTimer *_aliveCheckTimer;
    NSString *_hostName;
    int serverPort;
}

- (id)init
{
    self = [super init];
    if (self) {
        NSString *environmentPath = [[NSBundle mainBundle] pathForResource:@"environment" ofType:@"plist"];
        NSDictionary *environment = [NSDictionary dictionaryWithContentsOfFile:environmentPath];
        _hostName = [environment objectForKey:@"device_ip"];
        _handshaker = [[IDCCommnucator alloc] initWithHost:_hostName port:7246];
        _handshaker.delegate = self;
        _serverListener = [[AsyncSocket alloc] initWithDelegate:self];
        
    }
    return self;
}

- (void)dealloc
{
    [_serverListener release];
    [_serverCommunicator release];
    [_handshaker release];
    [_mainCommunicator release];
    [_aliveCheckTimer release];
    [_hostName release];
    [super dealloc];
}

- (void)start
{
    [_handshaker start];
    [_serverListener acceptOnPort:SERVER_PORT error:nil];
}

- (void)postConnectMessage
{
    SideSyncConnectMessage *message = [[[SideSyncConnectMessage alloc] init] autorelease];
    message.serverPort = SERVER_PORT;
    message.resolution = NSMakeSize(1080, 1920);
    message.type = SideSyncConnectTypePC;
    message.net = SideSyncConnectNetWireless;
    message.wifiMacAddress = @"40:0E:85:83:53:13";
    message.bluetoothMacAddress = @"BC:79:AD:ED:7F:D8";
    message.name = @"iMac";
    [_handshaker postMessage:message];
}

- (void)postAliveCheckMessage
{
    [_aliveCheckCommunicator start];
}

- (void)communicatorDidConnect:(IDCCommnucator *)sender
{
    if (sender == _handshaker) {
        [self postConnectMessage];
        
    } else if (sender == _mainCommunicator) {
        
    } else if (sender == _aliveCheckCommunicator) {
        AliveCheckMessage *message = [[[AliveCheckMessage alloc] init] autorelease];
        [_aliveCheckCommunicator postMessage:message];
    }
}

- (void)communicator:(IDCCommnucator *)sender didReceiveMessage:(IDCMessage *)message
{
    if ([[message className] isEqualToString:@"SideSyncConnectMessage"]) {
        SideSyncConnectMessage *connectMessage = (id)message;
        serverPort = connectMessage.serverPort;
        NSLog(@"Handshaking completed with port: %d", connectMessage.serverPort);
        _aliveCheckCommunicator = [[IDCCommnucator alloc] initWithHost:_hostName port:serverPort];
        _aliveCheckCommunicator.delegate = self;
        _aliveCheckTimer = [[NSTimer timerWithTimeInterval:9.0 target:self selector:@selector(postAliveCheckMessage) userInfo:nil repeats:YES] retain];
        [[NSRunLoop currentRunLoop] addTimer:_aliveCheckTimer forMode:NSRunLoopCommonModes];
        
    } else if ([[message className] isEqualToString:@"AliveCheckMessage"]) {
        [_aliveCheckCommunicator stop];
    }
}

- (void)onSocket:(AsyncSocket *)sock didAcceptNewSocket:(AsyncSocket *)newSocket
{
    if (_serverCommunicator)
        return;
    
    NSLog(@"Accepted");
    
    _serverCommunicator = [[IDCCommnucator alloc] initWithSocket:newSocket];
    _serverCommunicator.delegate = self;
}

@end
