//
//  AppDelegate.m
//  SSConTest
//
//  Created by mint on 13. 10. 4..
//  Copyright (c) 2013년 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "SideSyncConnectMessage.h"
#import "PSSTester.h"

@implementation AppDelegate
{
    PSSTester *pssTester;
}

@synthesize window = _window;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    pssTester = [[PSSTester alloc] init];
    [pssTester start];
}

@end
