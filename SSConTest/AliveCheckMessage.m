//
//  AliveCheckMessage.m
//  SSConTest
//
//  Created by Yoon Sungwook on 10/11/13.
//
//

#import "AliveCheckMessage.h"

@implementation AliveCheckMessage

+ (NSString *)mainCommand
{
    return @"ALIVE";
}

+ (NSString *)subCommand
{
    return @"ALIVE_CHECK";
}

- (NSData *)data
{
    self.parameter = @"";
    
    return [super data];
}

@end
