//
//  IDCCommnucator.m
//  SSConTest
//
//  Created by Yoon Sungwook on 10/11/13.
//
//

#import "IDCCommnucator.h"

@interface IDCCommnucator () <AsyncSocketDelegate>

@end

@implementation IDCCommnucator
{
    NSString *_hostName;
    int _port;
    AsyncSocket *_socket;
}

@synthesize sequenceNumber = _sequenceNumber;

- (id)initWithSocket:(AsyncSocket *)socket
{
    self = [super init];
    if (self) {
        _socket = socket;
        _socket.delegate = self;
    }
    return self;
}

- (id)initWithHost:(NSString *)hostName port:(int)port
{
    self = [super init];
    if (self) {
        _hostName = hostName.copy;
        _port = port;
        _socket = [[AsyncSocket alloc] initWithDelegate:self];
    }
    return self;
}

- (void)dealloc
{
    [_socket release];
    [_hostName release];
    [super dealloc];
}

- (void)start
{
    _sequenceNumber = 0;
    [_socket connectToHost:_hostName onPort:_port error:nil];
}

- (void)stop
{
    [_socket disconnect];
}

- (void)postMessage:(IDCMessage *)message
{
    message.sequenceNumber = self.sequenceNumber;
    _sequenceNumber++;
    NSData *data = [message data];
    
    UInt16 payloadLength = data.length;
    payloadLength = HTONS(payloadLength);
    
    NSMutableData *packet = [NSMutableData data];
    [packet appendBytes:&payloadLength length:2];
    [packet appendData:data];
    
    [_socket writeData:packet withTimeout:10.0 tag:1];
    NSLog(@"send: %@", [message stringMessage]);
}

- (void)onSocket:(AsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port
{
    NSLog(@"connected to %@:%d", host, port);
    if ([self.delegate respondsToSelector:@selector(communicatorDidConnect:)]) {
        [self.delegate communicatorDidConnect:self];
    }
    [_socket readDataWithTimeout:-1.0 tag:2];
}

- (void)onSocket:(AsyncSocket *)sock willDisconnectWithError:(NSError *)err
{
    NSLog(@"%@ willDisconnectWithError: %@", _socket, err);
}

- (void)onSocket:(AsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag
{
    NSData *payload = [NSData dataWithBytes:data.bytes + 2 length:data.length - 2];
    IDCMessage *message = [IDCMessage idcMessageWithData:payload];
    NSLog(@"read: %@", [message stringMessage]);
    if ([self.delegate respondsToSelector:@selector(communicator:didReceiveMessage:)]) {
        [self.delegate communicator:self didReceiveMessage:message];
    }
    [_socket readDataWithTimeout:-1.0 tag:2];
}

@end
