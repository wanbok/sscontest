#ifndef _EVENT_SOCKET_PACKET_FACTORY_H
#define _EVENT_SOCKET_PACKET_FACTORY_H

#include "MessageType.h"
#include "Common.h"
class SocketPacketFactory {

	public:
		static char* connectConfimed(int *packet_len);
		static char* keyboardEvent(int actionType, int encodeType, char data[4], int *packet_len);
		static char* mouseEvent(int eventType, int xPos, int yPos, int *packet_len);
		static char* settingWorkingDevice(bool state, int *packet_len);
		static char* clipboardStart(int clip_len,int *packet_len);
		static char* clipboardBuffer(char* data, int offset, int length,int *packet_len);
		static char* clipboardEnd(int *packet_len);
		static char* commonMessageStart(int request_id, c_uint64 clip_len, int *packet_len,bool isRequest);
		static char* commonMessageBuffer(char* data, int offset, int length, int *packet_len,bool isRequest);
		static char* commonMessageEnd(int *packet_len,bool isRequest);
		static char* fileSendStart(char* file_name, int file_name_length , c_uint64 file_len, c_uint64 data_offset, int *packet_len);
		static char* fileSendEnd(char* file_name,int file_name_length, int *packet_len);
		static char* fileSendBuffer(char* data, int length,int *packet_len);
		static char* fileSendCancel(int *packet_len);
		static char* fileSendFail(int *packet_len);
		static char* createSimplePacket(MessageType message_type, int *packet_size);
		static char* screenResolutionRequest(int target_version, int *packet_len);
		static char* screenResolution(char type, int width, int height, int *packet_len);
		static char* deviceNameRequest(int target_version, int *packet_len);
		static char* deviceName(char* device_name, int *packet_len);
		static char* commonRequestResponse(int request_id,char* message, int msg_len, bool isResponse, int *packet_len);
		static char* getHeartBeatPacket(int *packet_len);
		static char* getHeartBeatResponsePacket(int *packet_len);
	private:
		static char* createDummyPacket(MessageType message_type, int packet_size);
		static void putByte(char* buff, int start, char val);
		static void putInt(char* buff,int start,int val);
		static void putBuff(char* buff,int start, char* val, int len);
		static void putLong(char* buff,int start,c_uint64 val);
	
};

#endif
