#include "stdafx.h"

//#include "SocketPacketFactory.h"
#include "SendThreadLooper.h"
#include "Settings.h"
#include "ErrorCodes.h"

#define  LOG_TAG    "SendThreadLooper"
int SendThreadLooper::taskCount = 0;

SSendPacket::SSendPacket(char *packet, int packet_len)
{
	_packet = new char[packet_len];
	memcpy(_packet, packet, packet_len);
	_packet_len = packet_len;
}

SSendPacket::~SSendPacket()
{
	delete[] _packet;
}

SendTask::SendTask()
{
	is_set_obj = false;
	obj_len = 0;
	task_id = -1;
	task_type = -1;
	obj = NULL;
}

void SendTask::setObj(const char *_obj, c_uint64 objSize)
{
	if (objSize != 0)
	{
		obj = new char[objSize + 1];
		memset(obj, 0, (sizeof(char) * objSize));

		memcpy(obj, _obj, objSize);
		//obj[objSize]=0;
		is_set_obj = true;
		obj_len = objSize;
	}
	else
		obj = NULL;
}

char* SendTask::getObj()
{
	return obj;
}

c_uint64 SendTask::getObjLen()
{
	return obj_len;
}

void SendTask::destroy()
{
	//LOGD("Destroy task - id=%d", task_id);
	if (is_set_obj && obj)
	{
		delete[] obj;
		obj = NULL;
		is_set_obj = false;
	}
}

SendThreadLooper::SendThreadLooper(int instantID, bool isTCP)
{
	_packetMaker = NULL;
	__instantID = instantID;
	__pTaskList = new TASKLIST();
	__isSendThreadAlive = false;
	_isCancelled = false;
	_isUserCanceled = false;
	__skipWaitToSendReadyRequest = false;

	__isTCP = isTCP;
	__desIPAddr = NULL;
	InitializeCriticalSection(&cs);
	CREATE_SEMAPHORE(__semap);
	__readyToSendMutex = new PalMutex("ReadyToSend");
	__waitToSendSemap = new PalSemaphore(4);
}

SendThreadLooper::~SendThreadLooper()
{
	cancelTasks(false);

	__waitToSendSemap->signal();

	PAL_SLEEP_MS(100);

	SAFE_DELETE(__pTaskList);
	DESTROY_SEMAPHORE(__semap);

	SAFE_DELETE(__waitToSendSemap);

	SAFE_DELETE(__readyToSendMutex);
}

void SendThreadLooper::start(int sockid, bool security,
		char* desIPAddr, int desPort)
{
	LOGD("start(%d)[%d] - IN", sockid, __instantID);

	EnterCriticalSection(&cs);

	if (__isSendThreadAlive == true)
	{
		__isSendThreadAlive = false;

		//if (pthreadid) {
		RELEASE_SEMAPHORE(__semap);

		LOGD("start() - join thread - IN");

		pthread_join(__pthreadId, NULL);

		LOGD("start() - join thread - OUT");
		//}
	}

	__pcClientFd = sockid;

	__desIPAddr = desIPAddr;
	__desPort = desPort;
	__needWaitToSend = false;

	if (__isTCP == false && __desIPAddr)
	{
		struct addrinfo hints;
		char* desPortStr = new char[6]();
		int rv;

		sprintf(desPortStr, "%d", __desPort);

		memset(&hints, 0, sizeof hints);
		hints.ai_family = AF_INET;
		hints.ai_socktype = SOCK_DGRAM;

		if ((rv = getaddrinfo(__desIPAddr, desPortStr, &hints, &__adrInfo)) != 0)
		{
			LOGE("Error in getaddrinfo: %s\n", gai_strerror(rv));
		}
		else
		{
			LOGD("Get destination address successfully");
		}
	}
	LeaveCriticalSection(&cs);

	pthread_create(&__pthreadId, NULL, sendThreadMain, (void*) this);

	LOGD("start()[%d] - OUT", __instantID);
}

void SendThreadLooper::stop()
{
	LOGD( "stop()[%d] - IN - is_send_thread_alive = %s", __instantID, __isSendThreadAlive?"true":"false");

	if (__waitToSendSemap)
		__waitToSendSemap->signal();

	if (__isSendThreadAlive == true)
	{
		__isSendThreadAlive = false;

		RELEASE_SEMAPHORE(__semap);

		LOGD("stop() - join thread - IN");

		pthread_join(__pthreadId, NULL);

		LOGD("stop() - join thread - OUT");
	}
	else
	{
		RELEASE_SEMAPHORE(__semap);
	}

	LOGD("stop()[%d] - OUT", __instantID);
}

void SendThreadLooper::signalToSend()
{
	if (!__needWaitToSend)
		return;

	int count = 0;
	if (!Settings::getInstance()->isConnected())
		return;

	while (__waitToSendSemap && __waitToSendSemap->isSignaled() && count < 50)
	{
		count++;
		PAL_SLEEP_MS(10);
	}

	if (!__waitToSendSemap)
		return;

	__readyToSendMutex->lock();
	__waitToSendSemap->signal();
	__readyToSendMutex->unlock();
}
void SendThreadLooper::setWaitToSend(bool needWaitToSend)
{
	__needWaitToSend = needWaitToSend;
}

void SendThreadLooper::setSkipWaitToSendReadyRequest()
{
	__skipWaitToSendReadyRequest = true;
}

int SendThreadLooper::submitTask(SendTask* task)
{
	EnterCriticalSection(&cs);

	_isCancelled = false;

	taskCount++;
	task->task_id = taskCount;

	__pTaskList->push_back(*task);

	LeaveCriticalSection(&cs);

	RELEASE_SEMAPHORE(__semap);
	//Signal the looper to work
	return task->task_id;
}

int SendThreadLooper::cancelTasks(bool userCanceled)
{
	LOGD("cancelTasks()[%d] - IN", __instantID);
	_isCancelled = true;
	_isUserCanceled = userCanceled;
	EnterCriticalSection(&cs);

	if (__pTaskList && __pTaskList->size() > 0)
	{

		TASKLIST::iterator it = __pTaskList->begin();
		while (it != __pTaskList->end())
		{
			LOGD( "cancelTasks()[%d] - remove task %d", __instantID, it->task_id);
			it->destroy();
			it = __pTaskList->erase(it);
			if (__pTaskList->empty() == false)
			{
				it = __pTaskList->begin();
			}
			else
			{
				break;
			}
		}
	}
	LeaveCriticalSection(&cs);

	signalToSend();
	LOGD("cancelTasks()[%d] - OUT", __instantID);
	return 0;
}

void* SendThreadLooper::sendThreadMain(void *arg)
{
	SendThreadLooper *h = reinterpret_cast<SendThreadLooper *>(arg);
	h->sendThreadLoop();
	return 0;
}

void SendThreadLooper::sendThreadLoop()
{
	__isSendThreadAlive = true;
	while (__isSendThreadAlive)
	{
		int _task_count = 0;

		EnterCriticalSection(&cs);

		if (__pTaskList == NULL)
		{
			break;
		}

		_task_count = __pTaskList->size();

		if (_task_count == 0)
		{
			LeaveCriticalSection(&cs);
			WAIT_SEMAPHORE(__semap);
			continue;
		}

		if (!__isSendThreadAlive)
		{
			LeaveCriticalSection(&cs);
			break;
		}

		TASKLIST::iterator it = __pTaskList->begin();

		//Clone task
		SendTask* currTask = new SendTask();
		currTask->task_type = it->task_type;
		currTask->setObj(it->getObj(), it->getObjLen());
		currTask->arg1 = it->arg1;
		currTask->arg2 = it->arg2;

		//Remove from list
		it->destroy();
		if (__pTaskList->size() > 0) it = __pTaskList->erase(it);

		LeaveCriticalSection(&cs);

		bool isRequestPacket = false;

		switch (currTask->task_type)
		{
		case SocketData_Request:
			isRequestPacket = true;
		case SocketData_Response:
			{
				if(_packetMaker != NULL)
				{
					SSendPacket *packet = _packetMaker->CreateCustomPacket(currTask->arg1, currTask->getObj(), currTask->getObjLen(), isRequestPacket);
					int ret = send_packet(packet->GetPacket(), packet->GetLength());
					if (ret == -1)
					{
						currTask->destroy();
						SAFE_DELETE(currTask);
						delete packet;
						goto END_POINT;
					}
					delete packet;
				}
			}
			break;
		case SocketData_Packet:
			{
				int ret = send_packet(currTask->getObj(), currTask->getObjLen());
				if (ret == -1)
				{
					currTask->destroy();
					SAFE_DELETE(currTask);
					goto END_POINT;
				}
			}
			break;
		}

		currTask->destroy();
		SAFE_DELETE(currTask);
	}
END_POINT:
	LOGD("[%d]send_thread_loop() - OUT", __instantID);

	__isSendThreadAlive = false;
}

int SendThreadLooper::send_packet(char* packet_buff, int len)
{
	if (__pcClientFd == -1)
	{
		LOGE("Client is not connected");
		return -1;
	}

	int num = -1;

	if (__isTCP)
		num = send(__pcClientFd, (const char*) packet_buff, len, 0);
	else
		num = sendto(__pcClientFd, (const char*) packet_buff, len, 0, __adrInfo->ai_addr, __adrInfo->ai_addrlen);

	if (num == -1)
	{
		int lastError = WSAGetLastError();

		LOGE("Sent %d bytes to client, error : %d", num, lastError);

		perror("send");
	}

	return num;
}

bool SendThreadLooper::isRunning()
{
	return __isSendThreadAlive;
}
