#include "stdafx.h"

#include "SocketBufferStream.h"
#define  LOG_TAG    "SocketBufferStream"

SocketBufferStream::SocketBufferStream() {
	LOGD("Create SocketBufferStream");
	InitializeCriticalSection(&cs);
	
}

SocketBufferStream::~SocketBufferStream()
{
	DELETE_MUTEX;
}

c_uint32 SocketBufferStream::write(char * buff, c_uint32 len) {
	//char buffcp[len];
	//memcp(buffcp,buff,len);

	EnterCriticalSection(&cs);
	__stringStream.clear();
	__stringStream.write(buff, len);
	LeaveCriticalSection(&cs);
	return 0;
}

c_uint32 SocketBufferStream::read(char * buff, c_uint32 len, bool keep_position) {
	EnterCriticalSection(&cs);
	//save curr position
	c_uint32 _curr_pos = (c_uint32)__stringStream.tellg();
	//LOGD("SocketBufferStream:: _curr_pos = %d len=%d", _curr_pos, len);
	__stringStream.read(buff, len);
	c_uint32 _new_pos = (c_uint32)__stringStream.tellg();

	c_uint32 num = (_new_pos - _curr_pos);

	//LOGD("SocketBufferStream:: readsome %d bytes", num);
	//restore position;
	if (keep_position) {
		//LOGD("SocketBufferStream:: seekg to %d", _curr_pos);
		__stringStream.seekg(_curr_pos);
	}
	LeaveCriticalSection(&cs);

	return num;
}

c_uint32 SocketBufferStream::getPendingDataLen() {
	EnterCriticalSection(&cs);
	c_uint32 _curr_pos = (c_uint32)__stringStream.tellg();

	//Seek to end
	__stringStream.seekg(0, __stringStream.end);
	c_uint32 _end_pos = (c_uint32)__stringStream.tellg();
	c_uint32 _avai_length = _end_pos - _curr_pos;

	//Restore the position
	__stringStream.seekg(_curr_pos);
	LeaveCriticalSection(&cs);

	return _avai_length;
}

void SocketBufferStream::clear() {
	EnterCriticalSection(&cs);
	__stringStream.str("");
	LeaveCriticalSection(&cs);
}

