#pragma once

#include "SSideSyncSocket.h"

class SSideSyncMainSocket : public SSideSyncSocket
{
public:
	SSideSyncMainSocket();
	virtual ~SSideSyncMainSocket();

	virtual SideSyncSocketError Initialize(int port, BOOL isServer);
	virtual SideSyncSocketError Destroy();

	virtual SideSyncSocketError Connect(LPCTSTR ipAddr);
	virtual SideSyncSocketError Disconnect();

	//virtual SideSyncSocketError SendRequest(int deviceId, int sid, byte* msg, int msg_len);
	//virtual SideSyncSocketError SendResponse(int deviceId, int sid, byte* msg, int msg_len);
	//virtual SideSyncSocketError SendPacket(byte* data, int data_len);

	SideSyncSocketError ResetSID();
	SideSyncSocketError SendRequestPacket(byte* data, int data_len);

	virtual int			onRequestHeaderSize();
	virtual c_uint32	onRequestPacketSizeFromHeader(int headerSize, char* header);

	virtual SSendPacket* CreateCustomPacket(int request_id, char* data, c_uint64 len, bool isRequest);

	void onReceiveListener( int socketid, char* ipaddr, char* data, int data_len );
private:
	int _sid;
};
