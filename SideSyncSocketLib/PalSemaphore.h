#pragma once

#include "Common.h"
#include "Log.h"


class PalSemaphore
{
public:
	PalSemaphore(int id);
	~PalSemaphore();
	void signal();
	void wait(int timeout = 0);
	bool isSignaled();
	void prepareWait();

private:
	int __id;
	bool __isSignal;
	bool __isWaitingSignal;
	DECLARE_SEMAPHORE(__internalSemap);
	CRITICAL_SECTION cs;
};
