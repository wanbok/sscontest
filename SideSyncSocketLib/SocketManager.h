#ifndef _SERVER_SOCKET_H
#define _SERVER_SOCKET_H



#include "Log.h"
#include "Common.h"
#include "SocketBufferStream.h"
#include "SendThreadLooper.h"
#include "MessageType.h"

// #define EVENT_PORT 30830 // the port users will be connecting to
#define BACKLOG				10 // how many pending connections queue will hold
#define CONNECT_TIMEOUT_USB			(500)
#define CONNECT_TIMEOUT_WIFI		(8000)
#define CONNECT_TIMEOUT_INTERVAL	(500)
#define MOBILE_NET_INTERFACE		"wlan0"
#define MOBILE_WIDI_INTERFACE		"p2p-wlan0-0"
#define MOBILE_RNDIS_INTERFACE		"rndis0"

class SocketManager;
class INotifySocketEvent
{
	virtual void OnSocketDisconnected(SocketManager *sender, LPSTR connectedIP) {}
	virtual void OnSocketError(SocketManager *sender, LPSTR connectedIP, int errorIndex) {}
};

/**
 * Create and manage TCP Socket to communicate with other devices. Each instance of this class can be either a Server or Client
 * depend on the flag set in SideSyncFramework initialize \n
 * SideSync Framework employs 3 TCP Sockets: \n
 * -	One socket for event exchanging, i.e, Connect event, Keyboard event, mouse event (USB Mode),....\n
 * - 	Two sockets for file transferring
 */
class SocketManager
{
	public:
		SocketManager(int port, ISocketManagerListener* owner, IPacketMaker* ownerPacketMaker, int instanceID, bool isServer);
		~SocketManager();

		int		createSocket(char* ipaddr = "");
		int 	closeSocket();
		int 	getClientSocket();
		bool 	isSocketConnected();
		SendThreadLooper* getSendLooper();
		bool	isServer();
		void	setConnected(bool state);
		bool	isConnected();
		void	setConfirmState(bool state);
		bool	getConfirmState();
		bool	needSecurity();
		char*	getAuthenticationInfo(int& length);
		bool	isConnectedViaWiFi();
		bool	isReceiveThreadRunning();
		int 	getConnectionType();
		char* getConnectedIpAddr();
	protected:
		static void* connectThreadMain(void* arg);
		static void* receiveThreadMain(void* arg);

		int connectThreadFunc(void);
		void receiveThreadLoop(void);	

		int connectAsServer(void);
		int connectAsClient(void);
		int receiveData(char* buf);
	private:

		int __instantID;
		int __connectionType;

		ISocketManagerListener * __pOwner;
		IPacketMaker* __pOwnerPacketMaker;
		SocketBufferStream __socketBuffStream;
		SendThreadLooper *__pSendThreadLooper;
		int __sockFd, __clientFd; // listen on sock_fd, new connection on pc_client_fd
		pthread_t __pthreadReceive;
		pthread_t __pthreadConnect;

		int __port,__buffLen;
		bool __isServer;
		bool __isSocketConnected, __isSocketCreated, __isDeviceConnected,
			__isReceiveThreadRunning, __isConnectThreadRunning, __isConfirming;

		struct addrinfo __hints;
		struct sockaddr_storage __theirAddr; // connector's address information
        struct sockaddr_in __servAddr;
		socklen_t sin_size;			
		char __remoteIp[INET6_ADDRSTRLEN];
		char __serverIp[INET6_ADDRSTRLEN];
		char __accptedInterfaceIP[INET6_ADDRSTRLEN];

		void init(int port, ISocketManagerListener* owner);
		bool __needSecurity;
		char* __authenticationInfo;
		int __authenticationInfoLength;
		bool __connectedViaWifi;
		bool setAuthenticationInfo(char* ipAddress);
		char* getClientMacAddress(char* ipAddress);
		char* getLocalMacAddress(char* ipAddress);

		void determineConntectionType();
		void getInterfaceFromIP(char* ip, char* interfaceName);

		INotifySocketEvent _eventDelegate;
};

#endif
