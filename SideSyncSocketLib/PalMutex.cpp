#include "stdafx.h"

#include "PalMutex.h"


#define LOG_TAG "PalMutex"
PalMutex::PalMutex(const char* id, bool isShowLog)
{
	__id = new char[strlen(id)+1];
	__isShowLog = isShowLog;
	strcpy(__id, id);
	InitializeCriticalSection(&cs);
	CREATE_MUTEX_V(__internalMutex);
}
PalMutex::~PalMutex()
{
	SAFE_DELETE_ARRAY(__id);
	DELETE_MUTEX_V(__internalMutex);
	DELETE_MUTEX;
	__lockThreadMap.clear();
}
void PalMutex::lock()
{
	//Lock to check if the mutex is call by this thread ID
	LOCK_MUTEX_V(__internalMutex);
	std::map<c_uint32, bool>::iterator it;
	c_uint32 threadId = PAL_GET_THREAD_ID;
	it = __lockThreadMap.find(threadId);
	if (it!=__lockThreadMap.end())
	{
		if (it->second)
		{
			if (__isShowLog) LOGD("Thread [%d] is already locked the mutex",threadId);
			UNLOCK_MUTEX_V(__internalMutex);
			return;
		}
	}
	//End checking threadID in the Map
	UNLOCK_MUTEX_V(__internalMutex);


	//Lock this thread upon lock() API
	EnterCriticalSection(&cs);
	__lockThreadMap[threadId] = true;
	if (__isShowLog) LOGD("[%s] lock() - threadID:%d",__id,threadId);
}
void PalMutex::unlock()
{
	
	//Lock to remove this thread ID from the map
	LOCK_MUTEX_V(__internalMutex);
	c_uint32 threadId = PAL_GET_THREAD_ID;
	__lockThreadMap[threadId]=false; //Remove thread ID from Map	
	UNLOCK_MUTEX_V(__internalMutex);

	//unLock this thread upon lock() API
	LeaveCriticalSection(&cs);
	if (__isShowLog) LOGD("[%s] unlock()- threadID:%d",__id,threadId);
	
}

