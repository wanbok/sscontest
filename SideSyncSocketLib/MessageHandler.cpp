#include "stdafx.h"

#include "MessageHandler.h"

#define LOG_TAG "MessageHandler"
MessageHandler::MessageHandler(int id, IInternalEventListener* internalEventListener)
{
	InitializeCriticalSection(&cs);
	CREATE_SEMAPHORE(__semap);
	__instanceID = id;
	__pInternalEventListener = internalEventListener;
	__pEventList = new EVENT_LIST();
	__isStarted = true;
	pthread_create(&__pthreadId, NULL, threadMain, (void*) this);
	LOGD("MessageHandler[%d] is created",__instanceID);
}
MessageHandler::~MessageHandler()
{
	__isStarted = false;

	RELEASE_SEMAPHORE(__semap);

	EnterCriticalSection(&cs);
	__pEventList->clear();
	SAFE_DELETE(__pEventList);
	LeaveCriticalSection(&cs);

	PAL_SLEEP_MS(100);

	DELETE_MUTEX;
	DESTROY_SEMAPHORE(__semap);

	LOGD("~MessageHandler[%d] is destroy",__instanceID);
}

void MessageHandler::postMessage(EventMessage* event)
{
	EnterCriticalSection(&cs);
	if (__pEventList)
	{
		__pEventList->push_back(*event);
	}
	LeaveCriticalSection(&cs);

	RELEASE_SEMAPHORE(__semap);
}

void MessageHandler::clearAllMessage()
{
	EnterCriticalSection(&cs);
	if (__pEventList)
	{
		__pEventList->clear();
	}
	LeaveCriticalSection(&cs);
	RELEASE_SEMAPHORE(__semap);
}

void* MessageHandler::threadMain(void *arg)
{
	MessageHandler *h = reinterpret_cast<MessageHandler *>(arg);
	int ret = h->workThread();

	return 0;
}

int MessageHandler::workThread()
{
	LOGD("workThread()[%d]- IN", __instanceID);

	__isRuning = true;
	while (__isStarted)
	{
		int _task_count = 0;

		EnterCriticalSection(&cs);

		if (__pEventList == NULL)
		{
			break;
		}

		_task_count = __pEventList->size();

		if (_task_count == 0)
		{
			LeaveCriticalSection(&cs);
			WAIT_SEMAPHORE(__semap);
			continue;
		}

		if (!__isStarted)
		{
			LeaveCriticalSection(&cs);
			break;
		}

		EVENT_LIST::iterator it = __pEventList->begin();

		//Clone task
		EventMessage* currEvent = new EventMessage(it->eventID,it->arg1,it->arg2,it->msg);
		//currEvent->arg1 = it->arg1;
		//currEvent->arg2 = it->arg2;
		//currEvent->eventID = it->eventID;
		//currEvent->msg = it->msg;

		if (__pEventList->size() > 0)
			it = __pEventList->erase(it);

		LeaveCriticalSection(&cs);

		LOGD("workThread()[%d] - Processing event %d",__instanceID,currEvent->eventID);

		if (__pInternalEventListener)
			__pInternalEventListener->onInternalEventListener(currEvent->eventID, currEvent->arg1, currEvent->arg2,
					currEvent->msg);

		SAFE_DELETE(currEvent);
	}
	__isRuning = false;

	LOGD("workThread()[%d] - OUT", __instanceID);

	return 0;
}
