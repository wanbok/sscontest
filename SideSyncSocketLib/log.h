/*
 * Copyright (C) 2005 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//
// C/C++ logging functions.  See the logging documentation for API details.
//
// We'd like these to be available from C code (in case we import some from
// somewhere), so this has a C interface.
//
// The output will be correct when the log file is shared between multiple
// threads and/or multiple processes so long as the operating system
// supports O_APPEND.  These calls have mutex-protected data structures
// and so are NOT reentrant.  Do not use LOG in a signal handler.
//

#pragma once

#include <stdio.h>
#include <stdarg.h>
#include "Common.h"
#include "FileLog.h"
#ifndef LOG_TAG
#define  LOG_TAG    "SOCKET_TEST"
#endif
#define MAX_LOG_LINE_LEN 1024*5

#define DEBUGMODE 0
#define RELEASEMODE 1

inline void _TRACE_(bool isError, const char* tag, const char *pStr, ... )
{
	int mode = RELEASEMODE;

#ifdef _DEBUG
	mode = DEBUGMODE;
#endif
	
	if((mode == DEBUGMODE) || (FileLog::bIsLogListenerSet == true))
	{
		char pTmp[MAX_LOG_LINE_LEN]="";
		va_list arglist;
		sprintf(pTmp+strlen(pTmp), "[");
		sprintf(pTmp+strlen(pTmp), tag);
		sprintf(pTmp+strlen(pTmp), "]	");
		if (isError)
			sprintf(pTmp+strlen(pTmp), "<<ERROR>>	");

		va_start(arglist, pStr); 
		vsprintf(pTmp+strlen(pTmp), pStr, arglist);
		va_end(arglist);
		sprintf(pTmp+strlen(pTmp), "\r\n");

		TRACE(pTmp);
	

		if(FileLog::bIsLogListenerSet == true)
			FileLog::Print(pTmp);
	}
}

#ifndef LOGD
#if LOG_NDEBUG > 1
#define LOGD(...)   ((void)0)
#else
#define  LOGD(...) {_TRACE_(false,LOG_TAG,__VA_ARGS__);}
#endif
#endif

#ifndef LOGE
#if LOG_NDEBUG > 3
#define LOGE(...)   ((void)0)
#else
#define  LOGE(...) _TRACE_(true,LOG_TAG,__VA_ARGS__)
#endif
#endif
