#pragma once

class ISideSyncSocketEventListener
{
public:
	virtual void onConnected(int connectType, LPCTSTR ipAddr) {}
	virtual void onConnectFailed(int connectType, LPCTSTR ipAddr) {}
	virtual void onDisconnected(int connectType, LPCTSTR ipAddr) {}
	virtual void onConnectLagged(int connectType, LPCTSTR ipAddr) {}

	virtual void onReceiveRequest(int reqId, char* msg, int length) {}
	virtual void onReceiveResponse(int reqId, char* msg, int length) {}
	virtual void onError(int errorCode, char* msg) {}

	virtual ~ISideSyncSocketEventListener(void) {}
};
