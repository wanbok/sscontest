#include "stdafx.h"
#include "SSideSyncIDCSocket.h"

SSideSyncIDCSocket::SSideSyncIDCSocket()
{
}

SSideSyncIDCSocket::~SSideSyncIDCSocket()
{
}

SideSyncSocketError SSideSyncIDCSocket::Initialize( int port, BOOL isServer )
{
	return SSideSyncSocket::Initialize(port, isServer);
}

SideSyncSocketError SSideSyncIDCSocket::Destroy()
{
	return SSideSyncSocket::Destroy();
}

SideSyncSocketError SSideSyncIDCSocket::Connect( LPCTSTR ipAddr )
{
	return SSideSyncSocket::Connect(ipAddr);
}

SideSyncSocketError SSideSyncIDCSocket::Disconnect()
{
	return SSideSyncSocket::Disconnect();
}

SideSyncSocketError SSideSyncIDCSocket::SendPacket( byte* data, int data_len )
{
	return SSideSyncSocket::SendPacket(data, data_len);
}

SSendPacket* SSideSyncIDCSocket::CreatePacket( char* data, c_uint64 len )
{
	return NULL;
}

int SSideSyncIDCSocket::onRequestHeaderSize()
{
	return IDC_HEADER_SIZE;
}

c_uint32 SSideSyncIDCSocket::onRequestPacketSizeFromHeader(int headerSize, char* header)
{
	c_uint32 nSIze_1 = static_cast<c_uint32>(header[0]);
	c_uint32 nSIze_2 = static_cast<c_uint32>(header[1]);

	int nPacketSIze = static_cast<int>(nSIze_1 & 0xff) + static_cast<int>(nSIze_2 & 0xff);

	return nPacketSIze + IDC_HEADER_SIZE;
}

void SSideSyncIDCSocket::onReceiveListener(int socketid, char* ipaddr, char* data, int data_len)
{
	data[data_len] = 0;
	m_IDCCallback_Recv(g_pIDCParent, data);
}

void SSideSyncIDCSocket::SetParent(void* pParent)
{
	g_pIDCParent = pParent;
}

void SSideSyncIDCSocket::SetCallbackRecv(IDCcallback_Recv pIDCCallback)
{
	m_IDCCallback_Recv = pIDCCallback;
}