#ifndef _MESSAGE_HANDLER_H
#define _MESSAGE_HANDLER_H

#include <list>
#include "Common.h"
#include "Log.h"
#include "MessageType.h"

using namespace std;

struct EventMessage
{

public:
	EventMessage(int eventID, int arg1, c_uint64 arg2, char* msg)
	{
		this->eventID = eventID;
		this->arg1 = arg1;
		this->arg2 = arg2;
		this->msg = msg;
	}

	int eventID;
	int arg1;
	c_uint64 arg2;
	char* msg;
};


typedef list<EventMessage> EVENT_LIST;

class MessageHandler
{
public:
	MessageHandler(int id, IInternalEventListener* internalEventListener);
	~MessageHandler();
	void postMessage(EventMessage* event);
	void clearAllMessage();

protected:
	static void* threadMain(void *arg);
	int workThread();

private:

	int __instanceID;
	bool __isRuning;
	bool __isStarted;

	pthread_t __pthreadId;
	EVENT_LIST* __pEventList;
	pthread_t __pWorkThread;
	CRITICAL_SECTION cs;
	DECLARE_SEMAPHORE(__semap);
	IInternalEventListener* __pInternalEventListener;
};

#endif
