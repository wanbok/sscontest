#pragma once

#include "SSideSyncSocket.h"

typedef enum _DEVICE_KEY_TYPE
{
	DKT_MENU,
	DKT_HOME,
	DKT_BACK
}
DEVICE_KEY_TYPE;

class SSideSyncUibcSocket : public SSideSyncSocket
{
public:
	SSideSyncUibcSocket();
	virtual ~SSideSyncUibcSocket();

	virtual SideSyncSocketError Initialize(int port, BOOL isServer);
	virtual SideSyncSocketError Destroy();

	virtual SideSyncSocketError Connect(LPCTSTR ipAddr);
	virtual SideSyncSocketError Disconnect();

	virtual SideSyncSocketError SendRequest(int deviceId, int sid, byte* msg, int msg_len) { return OK; }  // UIBC는 사용 안함
	virtual SideSyncSocketError SendResponse(int deviceId, int sid, byte* msg, int msg_len) { return OK; } // UIBC는 사용 안함
	virtual SideSyncSocketError SendPacket(byte* data, int data_len);

	// UIBC Methods
	SideSyncSocketError SendMouseDownEvent(int x, int y);
	SideSyncSocketError SendMouseUpEvent(int x, int y);
	SideSyncSocketError SendMouseMoveEvent(int x, int y);
	SideSyncSocketError SendDeviceKeyDownEvent(DEVICE_KEY_TYPE type);
	SideSyncSocketError SendDeviceKeyUpEvent(DEVICE_KEY_TYPE type);

	//UIBC는 Receive 처리 안함
	virtual int			onRequestHeaderSize() { return 0; }
	virtual c_uint32	onRequestPacketSizeFromHeader(int headerSize, char* header) { return 0; }

private:
	SideSyncSocketError SendMouseEvent(int inputType, int x, int y);
	SideSyncSocketError SendDeviceKeyEvent(int inputType, DEVICE_KEY_TYPE keyType);

	LPBYTE CreateMouseEventPacket(int inputType, int x, int y, int& packetLength); // For Left Mouse Down/UP/Move
	LPBYTE CreateKeyEventPacket(int inputType, UCHAR key1, UCHAR key2, int& packetLength); // For Left Mouse Down/UP/Move
	void DeletePacket(LPBYTE packet);

	void FillUibcHeader(LPBYTE data, int packetLength);
	void FillUibcGenericInputBodyHeader(LPBYTE data, int inputType, int bodyLength);

	void GetDeviceKeyCode(DEVICE_KEY_TYPE type, UCHAR& key1, UCHAR& key2);
};
