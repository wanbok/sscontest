#include "stdafx.h"

#include "common.h"
#include "log.h"

#define LOG_TAG "common"
#define MAX_LOG_LINE_LEN 1024*2
//

void *get_in_addr(struct sockaddr *sa)
{
	if (sa->sa_family == AF_INET)
	{
		return &(((struct sockaddr_in*) sa)->sin_addr);
	}
	return &(((struct sockaddr_in6*) sa)->sin6_addr);
}



int byte2Int(char* src, int startIdx, bool big_endian)
{

	int result = 0;
	int n = 0;
	if (big_endian == true)
	{
		for (n = 0; n < sizeof(result); n++)
			result = (result << 8) + (unsigned char)src[startIdx + n];
	}
	else
	{
		for (n = sizeof(result) - 1; n >= 0; n--)
			result = (result << 8) + (unsigned char)src[startIdx + n];
	}

	return result;
}

c_uint64 byte2Long(char* src, int startIdx, bool big_endian)
{

	c_uint64 result = 0UL;
	int n = 0;

	if (big_endian == true)
	{
		for (n = 0; n < 8; n++)
			result = (result << 8) + (unsigned char)src[startIdx + n];
	}
	else
	{
		for (n = 8 - 1; n >= 0; n--)
			result = (result << 8) + (unsigned char)src[startIdx + n];
	}

	return result;
}

wchar_t * utf8_to_unicode_n(const char *pUtf8)
{
	if(pUtf8 == NULL)
	{
		return NULL;
	}

	int nLen = MultiByteToWideChar (CP_UTF8, 0, pUtf8, -1, NULL, 0);    //converted length (including \0)
	if (nLen <= 0) 
	{
		return NULL;
	}

	wchar_t * pUnicode = new wchar_t[nLen];
	if(!MultiByteToWideChar(CP_UTF8, 0, pUtf8, -1, pUnicode, nLen))
	{
		delete[] pUnicode;
		return NULL;
	}

	return pUnicode;
}

char* unicode_to_utf8_n(const wchar_t *pUnicode)
{
	if(pUnicode == NULL)
	{
		return NULL;
	}

	int nLen = WideCharToMultiByte (CP_UTF8, 0, pUnicode, -1, NULL, 0, NULL, NULL);
	if (nLen <= 0) 
	{
		return NULL;
	}

	char* pUtf8 = new char[nLen];
	if(!WideCharToMultiByte(CP_UTF8, 0, pUnicode, -1, pUtf8, nLen, NULL, NULL))
	{
		delete[] pUtf8;
		return NULL;
	}

	return pUtf8;
}

char* wchar_to_char(const wchar_t *pUnicode)
{
	if(pUnicode == NULL)
	{
		return NULL;
	}

	//입력받은 wchar_t 변수의 길이를 구함
    int nLen = WideCharToMultiByte(CP_ACP, 0, pUnicode, -1, NULL, 0, NULL, NULL);
	if (nLen <= 0) 
	{
		return NULL;
	}

    //char* 메모리 할당
    char* pStr = new char[nLen];

    //형 변환 
    if(!WideCharToMultiByte(CP_ACP, 0, pUnicode, -1, pStr, nLen, NULL, NULL))
	{
		delete[] pStr;
		return NULL;
	}
 
    return pStr;
}

char* copyStr(const char* source, int len)
{
	if (len==-1) len = strlen(source);
	
	char* _cpy = new char[len+1]();
	memcpy(_cpy, source, len);
	_cpy[len] = '\0';
	return _cpy;

}

unsigned int PAL_FILE_READ(PAL_FILE _file, char* _buffer, int _nSize)
{
#ifdef _WIN32
	return fread(_buffer, 1, _nSize, _file);
#else
	return read(_file, _buffer, _nSize);
#endif
}
unsigned int PAL_FILE_WRITE(PAL_FILE _file, char* _buffer, int _nSize)
{
#ifdef _WIN32
	return fwrite(_buffer, 1, _nSize, _file);
#else
	return write(_file, _buffer, _nSize);
#endif
}

int PAL_FILE_SEEK(PAL_FILE _file, c_uint64 _seek_pos, int _seek_dir)
{
#ifdef _WIN32
	return _fseeki64(_file, _seek_pos, _seek_dir);
#else
	return lseek64 (_file, _seek_pos, _seek_dir);
#endif
}

c_uint64 PAL_FILE_TELL(PAL_FILE _file)
{
#ifdef _WIN32
	return _ftelli64(_file);
#else
	return lseek64(_file, 0, SEEK_END);
#endif
}

PAL_FILE PAL_FILE_OPEN(const char* _path, FileOperationMode _mode)
{
#ifdef _WIN32
	int utf8_path_len = strlen(_path);
	char* utf8_file_path = new char[utf8_path_len+1];
	strcpy(utf8_file_path, _path);

	wchar_t* uni_file_path = utf8_to_unicode_n(utf8_file_path);
	if (uni_file_path == NULL)
	{
		LOGE("cannot convert string from utf8 to unicode");
		return 0;
	}
	PAL_FILE retValue;
	
	if(_mode == FileOperationMode::MODE_READ)
		retValue = _wfopen(uni_file_path, _T("rb"));
	else
		retValue = _wfopen(uni_file_path, _T("wb+"));

	if (uni_file_path)
		delete[] uni_file_path;

	if (utf8_file_path)
		delete[] utf8_file_path;

	return retValue;
#else
	PAL_FILE retValue;
	
	if(_mode == MODE_READ)
		retValue = open(_path, O_LARGEFILE | O_RDONLY);
	else
		retValue = open(_path, O_LARGEFILE | O_CREAT | O_WRONLY);

	return retValue;
#endif
}

void PAL_FILE_CLOSE(PAL_FILE _file)
{
#ifdef _WIN32
	fclose(_file);
#else
	close(_file);
#endif
}

c_uint64 mergeTo64(int part1, int part2)
{
	unsigned int _part1 = (unsigned int)part1;
	unsigned int _part2 = (unsigned int)part2;

	c_uint64 number = 0;
	number = _part1;
	number = (number << 32) + _part2; 

	return number;
}
void splitTo32(c_uint64 number, unsigned int* part1, unsigned int* part2)
{
	*part1 = (number >> 32) & 0xFFFFFFFF;
	*part2 = number & 0xFFFFFFFF;
}

c_uint64 getCurrTimeMs()
{
	// 주) 해당 결과값은 GetTickCount() 또는 GetTickCount64()의 결과값과 다릅니다.
	//     반드시 경과시간의 측정용으로만 사용하세요.
	LARGE_INTEGER perfFreq, perfCnt;
	QueryPerformanceFrequency(&perfFreq);
	QueryPerformanceCounter(&perfCnt);
	return perfCnt.QuadPart * 1000 / perfFreq.QuadPart;
}
