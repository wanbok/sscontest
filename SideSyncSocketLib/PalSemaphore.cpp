#include "stdafx.h"

#include "PalSemaphore.h"

#define  LOG_TAG    "PalSemaphore"
PalSemaphore::PalSemaphore(int id)
{
	__id = id;
	__isSignal = true;
	__isWaitingSignal = false;
	CREATE_SEMAPHORE(__internalSemap);
	InitializeCriticalSection(&cs);
}

PalSemaphore::~PalSemaphore()
{
	DESTROY_SEMAPHORE(__internalSemap);
	DELETE_MUTEX;
}

void PalSemaphore::signal()
{
	EnterCriticalSection(&cs);
	if (!__isSignal||__isWaitingSignal)
	{
		__isSignal = true;
		LOGD("Signal semaphore[%d]",__id);
		RELEASE_SEMAPHORE(__internalSemap);
		__isWaitingSignal = false;
	}
	LeaveCriticalSection(&cs);
}

void PalSemaphore::prepareWait()
{
	EnterCriticalSection(&cs);
	if (__isSignal)
	{
		LOGD("PrepareWait semaphore[%d]",__id);
		__isSignal = false;
		__isWaitingSignal = true;
	}
	LeaveCriticalSection(&cs);
}
void PalSemaphore::wait(int timeout)
{

	if (timeout>0)
	{
		LOGD("Wait semaphore[%d] - timeout: %d",__id, timeout);
		WAIT_SEMAPHORE_TIMEOUT(__internalSemap,timeout);
	}
	else
	{
		LOGD("Wait semaphore[%d]",__id);
		WAIT_SEMAPHORE(__internalSemap);
	}

}

bool PalSemaphore::isSignaled()
{
	return __isSignal;
}
