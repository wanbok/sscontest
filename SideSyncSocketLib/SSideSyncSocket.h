#pragma once

#include "ErrorCodes.h"
#include "SocketManager.h"
#include "ISideSyncSocketEventListener.h"

typedef enum _SIDESYNC_SOCKET_MODE
{
	SSM_CLIENT,
	SSM_SERVER
} SIDESYNC_SOCKET_MODE;

class SSideSyncSocket : ISocketManagerListener, IPacketMaker
{
public:
	SSideSyncSocket();
	virtual ~SSideSyncSocket();

	void SetEventListener(ISideSyncSocketEventListener* listener);

	virtual SideSyncSocketError Initialize(int port, BOOL isServer);
	virtual SideSyncSocketError Destroy();

	virtual SideSyncSocketError Connect(LPCTSTR ipAddr);
	virtual SideSyncSocketError Disconnect();

	virtual SideSyncSocketError SendRequest(int deviceId, int sid, byte* msg, int msg_len);
	virtual SideSyncSocketError SendResponse(int deviceId, int sid, byte* msg, int msg_len);
	virtual SideSyncSocketError SendPacket(byte* data, int data_len);

	virtual void onAcceptListener(int socketid, char* ipaddr);
	virtual void onConnectedListener(int socketid, char* ipaddr);
	virtual void onConnectionFailedListener(int socketid, char* ipaddr);
	virtual void onReceiveListener(int socketid, char* ipaddr, char* data, int data_len);
	virtual void onClientCloseListener(int socketid, char* ipaddr);
	virtual void onSocketDestroy();
	
	virtual SSendPacket* CreateCustomPacket(int request_id, char* data, c_uint64 len, bool isRequest) { return NULL; }

protected:
	ISideSyncSocketEventListener* _socketEventListener;

private:
	void CleanUp();

	SocketManager* _sockMgr;
	SIDESYNC_SOCKET_MODE _socketMode;

	//DECLARE_SEMAPHORE(_semaConfirmConnect);
	DECLARE_SEMAPHORE(_semaDestroy);
};
