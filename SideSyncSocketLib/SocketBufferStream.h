#ifndef _SOCKET_BUFFER_STREAM_H
#define _SOCKET_BUFFER_STREAM_H

#include <string>       // std::string
#include <iostream>     // std::cout
#include <sstream>      // std::stringstream
#include "common.h"
#include "Log.h"

class SocketBufferStream
{
	public:
		SocketBufferStream();
		~SocketBufferStream();	
		c_uint32 write(char* buff, c_uint32 len);
		c_uint32 read(char* buff, c_uint32 len,bool keep_position = false);
		c_uint32 getPendingDataLen();
		void clear();
		bool isDataAvailable();
	private:
		std::stringstream __stringStream;		
		CRITICAL_SECTION cs;
		bool __isAvailable;
};

#endif
