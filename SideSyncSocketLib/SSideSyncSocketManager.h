#pragma once

#include "ErrorCodes.h"
#include "SocketManager.h"
#include "MessageHandler.h"
#include "packetParser.h"

typedef enum _SIDESYNC_SOCKET_MODE
{
	SSM_CLIENT,
	SSM_SERVER
} SIDESYNC_SOCKET_MODE;

class SSideSyncSocketManager : IInternalEventListener, ISocketManagerListener
{
public:
	SSideSyncSocketManager(void);
	virtual ~SSideSyncSocketManager(void);

	SideSyncSocketError Initialize(int port, BOOL isServer);
	SideSyncSocketError Destroy();

	SideSyncSocketError Connect(LPCTSTR ipAddr);
	SideSyncSocketError Disconnect();

	SideSyncSocketError sendCommonRequest(int requestId, const char* msg, int msg_len);
	SideSyncSocketError sendCommonResponse(int requestId, const char* msg, int msg_len);

	virtual void onInternalEventListener(int eventID, int arg1, c_uint64 arg2, char* msg);

	virtual void onAcceptListener(int socketid, char* ipaddr);
	virtual void onConnectedListener(int socketid, char* ipaddr);
	virtual void onConnectionFailedListener(int socketid, char* ipaddr);
	virtual void onReceiveListener(int socketid, char* ipaddr, char* data, int data_len);
	virtual void onClientCloseListener(int socketid, char* ipaddr);
	virtual void onSocketDestroy();

protected:
	int SendConfirmConnected();

private:
	void CleanUp();
	int sendHeartBeatResponse();
	int receiveCommonMessage(PacketParser* packetParser, bool isRequest);

	SocketManager* _sockMgr;
	MessageHandler* _msgHandler;
	SIDESYNC_SOCKET_MODE _socketMode;

	// Common Message ����
	char* _msgBuf;
	int _commonMsgId;
	BOOL _isReceivingCommonMsg;
	int _receivedCommonMsgLen;

	DECLARE_SEMAPHORE(_semaConfirmConnect);
	DECLARE_SEMAPHORE(_semaDestroy);

	BOOL _isConnecting;
};
