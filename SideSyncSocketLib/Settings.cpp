#include "stdafx.h"

#include "Settings.h"

#define LOG_TAG "Settings"

Settings* Settings::__instance = NULL;

Settings::Settings()
{
	LOGD("<<Create Settings instance>>");
	__receiveFolder = NULL;
	__myVersion = PROTOCOL_VERSION;
	__connectedVersion = PROTOCOL_VERSION; //As default;
	__networkRTT = 0;
	time(&last_event);
	bSlowDownFileTransferring = false;
}

Settings::~Settings()
{
	LOGD("<<Setting instance is destroyed>>");
	SAFE_DELETE_ARRAY(__receiveFolder);
}

Settings* Settings::getInstance()
{
	if (__instance == NULL)
	{

		__instance = new Settings();
	}

	return __instance;
}

void Settings::destroyInstance()
{
	SAFE_DELETE(__instance);
}

bool Settings::isConnected()
{
	return __isConnected;
}

void Settings::setIsConnected(bool isconnected)
{
	__isConnected = isconnected;
}


void Settings::setReceiveFolder(const char* folder)
{
	SAFE_DELETE_ARRAY(__receiveFolder);
	if (folder)
	{
		__receiveFolder = new char[strlen(folder) + 1];
		strcpy(__receiveFolder, folder);
	}
}

const char* Settings::getReceiveFolder()
{
	return __receiveFolder;
}

int Settings::getMyVersion()
{
	return __myVersion;
}

int Settings::getConnectedProtocolVer()
{
	return __connectedVersion;
}

void Settings::setConnectedProtocolVer(int connectedVersion)
{
	__connectedVersion = connectedVersion;
}

c_uint64 Settings::getNetworkRTT()
{
	return __networkRTT;
}

void Settings::setNetworkRTT(c_uint64 value)
{
	__networkRTT = value;
}
void Settings::setLastTime()
{
	time(&last_event);
}

time_t* Settings::getLastTime()
{
	return &last_event;
}
