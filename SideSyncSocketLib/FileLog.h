#pragma once

#include "ISideSyncLogListener.h"

using namespace std;

class FileLog
{	
public:
	FileLog();
	DWORD _senderThread;
	HANDLE _threadHandle;
	static void* senderThreadMain(void* arg);
	int senderThreadFunc(void);
	static bool bIsLogListenerSet;
	//ILogEventListener* mSideSyncLogListener;
	//static void setLogListener(ILogEventListener* pListener);
	static void Print(const char *pStr);
};
