#include "stdafx.h"

#include <stdio.h>
#include <stdlib.h>

#include <errno.h>
#include <string.h>
#include <malloc.h>
#include "Settings.h"
#include "PacketParser.h"
#include "SocketPacketFactory.h"

#define  LOG_TAG    "SocketPacketFactory"
const int PACKET_FIXED_HEADER = HEADER_SIZE;

char* SocketPacketFactory::connectConfimed(int *packet_len)
{
	*packet_len = PACKET_FIXED_HEADER + 1;
	char* buff = createDummyPacket(CONFIRM_CONNECTED, *packet_len);
	buff[5] = 0x1; //data

	return buff;
}

char* SocketPacketFactory::getHeartBeatPacket(int *packet_len)
{
	*packet_len = PACKET_FIXED_HEADER + 1;
	char* buff = createDummyPacket(HEARTBEAT, *packet_len);
	buff[5] = 0x1; //data

	return buff;
}

char* SocketPacketFactory::getHeartBeatResponsePacket(int *packet_len)
{
	*packet_len = PACKET_FIXED_HEADER + 1;
	char* buff = createDummyPacket(HEARTBEAT_RESPONSE, *packet_len);
	buff[5] = 0x1; //data

	return buff;
}

char* SocketPacketFactory::keyboardEvent(int actionType, int encodeType, char data[4], int *packet_len)
{
	*packet_len = PACKET_FIXED_HEADER + SIZE_OF_BYTE + SIZE_OF_BYTE + SIZE_OF_BYTE + SIZE_OF_INT; // 11 bytes
	char* buff = createDummyPacket(CONTROL_KEYBOARD_EVENT, *packet_len);

	putByte(buff, PACKET_FIXED_HEADER, 0);
	putByte(buff, PACKET_FIXED_HEADER + SIZE_OF_BYTE, actionType);
	putByte(buff, PACKET_FIXED_HEADER + SIZE_OF_BYTE + SIZE_OF_BYTE, encodeType);
	putBuff(buff, PACKET_FIXED_HEADER + SIZE_OF_BYTE + SIZE_OF_BYTE + SIZE_OF_BYTE, data, 4);

	return buff;
}

char* SocketPacketFactory::mouseEvent(int eventType, int xPos, int yPos, int *packet_len)
{
	*packet_len = PACKET_FIXED_HEADER + SIZE_OF_BYTE + SIZE_OF_BYTE + SIZE_OF_INT + SIZE_OF_INT; // 14 bytes
	char* buff = createDummyPacket(CONTROL_MOUSE_EVENT, *packet_len);

	putByte(buff, PACKET_FIXED_HEADER, 0);
	putByte(buff, PACKET_FIXED_HEADER + SIZE_OF_BYTE, eventType);
	putInt(buff, PACKET_FIXED_HEADER + SIZE_OF_BYTE + SIZE_OF_BYTE, xPos);
	putInt(buff, PACKET_FIXED_HEADER + SIZE_OF_BYTE + SIZE_OF_BYTE + SIZE_OF_INT, yPos);

	return buff;
}

char* SocketPacketFactory::settingWorkingDevice(bool state, int *packet_len)
{
	*packet_len = PACKET_FIXED_HEADER + SIZE_OF_BYTE; // 5 bytes
	char* buff = createDummyPacket(CHANGE_WORKING_DEVICE_NOTIFICATION, *packet_len);

	putByte(buff, PACKET_FIXED_HEADER, state == true ? 1 : 0);

	return buff;
}

char* SocketPacketFactory::fileSendStart(char* file_name, int file_name_length, c_uint64 file_len, c_uint64 data_offset,
		int *packet_len)
{
	//LOGD("sizeof(taskId)=%d sizeof(file_len) = %d file_name_len =%d \n", sizeof(taskId), sizeof(file_len), file_name_len);
	*packet_len = PACKET_FIXED_HEADER + SIZE_OF_LONG + SIZE_OF_LONG + file_name_length;
	char* buff = createDummyPacket(FILE_TRANSMISSION_START, *packet_len);

	//putInt(buff, PACKET_FIXED_HEADER, taskId); //task id
	putLong(buff, PACKET_FIXED_HEADER, file_len); //file_len

	if (Settings::getInstance()->getConnectedProtocolVer() >= 2)
	{
		putLong(buff, PACKET_FIXED_HEADER + SIZE_OF_LONG, data_offset); //data offset
		putBuff(buff, PACKET_FIXED_HEADER + SIZE_OF_LONG + SIZE_OF_LONG, file_name, file_name_length); //file name
	}
	else
	{
		putBuff(buff, PACKET_FIXED_HEADER + SIZE_OF_LONG, file_name, file_name_length); //file name
	}

	return buff;
}

char* SocketPacketFactory::fileSendEnd(char* file_name, int file_name_length, int *packet_len)
{
	//LOGD("sizeof(taskId)=%d sizeof(file_len) = %d file_name_len =%d \n", sizeof(taskId), sizeof(file_len), file_name_len);
	*packet_len = PACKET_FIXED_HEADER + file_name_length;
	char* buff = createDummyPacket(FILE_TRANSMISSION_END, *packet_len);

	putBuff(buff, PACKET_FIXED_HEADER, file_name, file_name_length); //file name

	return buff;
}

char* SocketPacketFactory::fileSendBuffer(char* data, int length, int *packet_len)
{

	*packet_len = PACKET_FIXED_HEADER + length;
	char* buff = createDummyPacket(FILE_TRANSMISSION_STATUS, *packet_len);

	//putInt(buff, PACKET_FIXED_HEADER, taskId); //task id
	//putInt(buff, PACKET_FIXED_HEADER+sizeof(taskId), length); //buff length
	putBuff(buff, PACKET_FIXED_HEADER, data, length);

	return buff;
}

char* SocketPacketFactory::fileSendCancel(int *packet_len)
{
	*packet_len = PACKET_FIXED_HEADER;
	char* buff = createDummyPacket(FILE_TRANSMISSION_SEND_CANCELED, *packet_len);

	//putInt(buff, PACKET_FIXED_HEADER, taskId); //task id

	return buff;
}

char* SocketPacketFactory::fileSendFail(int *packet_len)
{
	*packet_len = PACKET_FIXED_HEADER;
	char* buff = createDummyPacket(FILE_TRANSMISSION_FAILED, *packet_len);

	//putInt(buff, PACKET_FIXED_HEADER, taskId); //task id

	return buff;
}

char* SocketPacketFactory::clipboardStart(int clip_len, int *packet_len)
{
	*packet_len = PACKET_FIXED_HEADER + sizeof(clip_len);
	char* buff = createDummyPacket(DATA_CLIPBOARD_START, *packet_len);

	putInt(buff, PACKET_FIXED_HEADER, clip_len); //clipoard size

	return buff;
}

char* SocketPacketFactory::clipboardBuffer(char* data, int offset, int length, int *packet_len)
{

	*packet_len = PACKET_FIXED_HEADER + length;
	char* buff = createDummyPacket(DATA_CLIPBOARD_BUFFER, *packet_len);
	putBuff(buff, PACKET_FIXED_HEADER, data + offset, length);

	return buff;
}

char* SocketPacketFactory::clipboardEnd(int *packet_len)
{
	*packet_len = PACKET_FIXED_HEADER;
	char* buff = createDummyPacket(DATA_CLIPBOARD_END, *packet_len);

	return buff;
}

char* SocketPacketFactory::commonMessageStart(int request_id, c_uint64 clip_len, int *packet_len, bool isRequest)
{
	*packet_len = PACKET_FIXED_HEADER + sizeof(clip_len)+SIZE_OF_LONG;
	char* buff = createDummyPacket(isRequest?DATA_COMMON_REQUEST_START:DATA_COMMON_RESPONSE_START, *packet_len);
	putInt(buff, PACKET_FIXED_HEADER, request_id);
	putLong(buff, PACKET_FIXED_HEADER+SIZE_OF_INT, clip_len); //common message size

	return buff;
}

char* SocketPacketFactory::commonMessageBuffer(char* data, int offset, int length, int *packet_len,bool isRequest)
{

	*packet_len = PACKET_FIXED_HEADER + length;
	char* buff = createDummyPacket(isRequest?DATA_COMMON_REQUEST_BUFFER:DATA_COMMON_RESPONSE_BUFFER, *packet_len);
	putBuff(buff, PACKET_FIXED_HEADER, data + offset, length);

	return buff;
}

char* SocketPacketFactory::commonMessageEnd(int *packet_len,bool isRequest)
{
	*packet_len = PACKET_FIXED_HEADER;
	char* buff = createDummyPacket(isRequest?DATA_COMMON_REQUEST_END:DATA_COMMON_RESPONSE_END, *packet_len);

	return buff;
}

char* SocketPacketFactory::screenResolutionRequest(int target_version, int *packet_len)
{
	*packet_len = PACKET_FIXED_HEADER;
	char* buff = 0;

	if (target_version == 0)
		buff = createDummyPacket(DISPLAY_RESOLUTION, *packet_len);
	else
		buff = createDummyPacket(DISPLAY_RESOLUTION_REQUEST, *packet_len);

	return buff;
}

char* SocketPacketFactory::screenResolution(char type, int width, int height, int *packet_len)
{
	*packet_len = PACKET_FIXED_HEADER + sizeof(type) + 2 * sizeof(height);
	char* buff = createDummyPacket(DISPLAY_RESOLUTION, *packet_len);

	buff[PACKET_FIXED_HEADER] = type; //Type
	//Width
	putInt(buff, PACKET_FIXED_HEADER + 1, width);
	putInt(buff, PACKET_FIXED_HEADER + 5, height);

	return buff;
}

char* SocketPacketFactory::deviceNameRequest(int target_version, int *packet_len)
{
	*packet_len = PACKET_FIXED_HEADER;
	char* buff = 0;

	if (target_version == 0)
		buff = createDummyPacket(DEVICE_NAME, *packet_len);
	else
		buff = createDummyPacket(DEVICE_NAME_REQUEST, *packet_len);

	return buff;
}

char* SocketPacketFactory::deviceName(char* device_name, int *packet_len)
{
	*packet_len = PACKET_FIXED_HEADER + strlen(device_name);
	char* buff = createDummyPacket(DEVICE_NAME, *packet_len);

	putBuff(buff, PACKET_FIXED_HEADER, device_name, strlen(device_name));

	return buff;
}

char* SocketPacketFactory::createDummyPacket(MessageType message_type, int packet_size)
{
	//int max_len = SIDESYNC_EVENT_PACKET_BUFFER_SIZE;
	char* buff;
	//= new char[max_len];
	buff = new char[packet_size + 1]();
	//memset(buff,0x0,packet_size);

	buff[0] = PROTOCOL_VERSION;
	buff[1] = (unsigned char) message_type; //type
	buff[2] = ((packet_size >> 8) & 0xFF); //packet size 1
	buff[3] = ((packet_size >> 0) & 0xFF); //packet size 2
	//buff[4] =  0x0;
	return buff;
}

char* SocketPacketFactory::createSimplePacket(MessageType message_type, int *packet_size)
{
	*packet_size = PACKET_FIXED_HEADER;
	return createDummyPacket(message_type, *packet_size);

}

void SocketPacketFactory::putByte(char* buff, int start, char val)
{
	if (start < 0)
		return;

	buff[start] = val;
}

void SocketPacketFactory::putInt(char* buff, int start, int val)
{
	if (start < 0)
		return;
	/*
	 int size_val = sizeof(val);
	 for (int i=0;i<size_val;i++)
	 {
	 buff[start+i] = ((val >> ((size_val-1-i)*8)) & 0xFF);
	 }*/

	buff[start] = ((val >> 24) & 0xFF);
	buff[start + 1] = ((val >> 16) & 0xFF);
	buff[start + 2] = ((val >> 8) & 0xFF);
	buff[start + 3] = ((val >> 0) & 0xFF);
}

void SocketPacketFactory::putBuff(char* buff, int start, char* val, int len)
{
	if (start < 0)
		return;

	memcpy(buff + start, val, len);

}

void SocketPacketFactory::putLong(char* buff, int start, c_uint64 val)
{
	if (start < 0)
		return;
	c_uint64 _val = val;

	buff[start] = ((_val >> 56) & 0xFF);
	buff[start + 1] = ((_val >> 48) & 0xFF);
	buff[start + 2] = ((_val >> 40) & 0xFF);
	buff[start + 3] = ((_val >> 32) & 0xFF);
	buff[start + 4] = ((_val >> 24) & 0xFF);
	buff[start + 5] = ((_val >> 16) & 0xFF);
	buff[start + 6] = ((_val >> 8) & 0xFF);
	buff[start + 7] = ((_val >> 0) & 0xFF);

}

