#include "stdafx.h"
#include "SSideSyncMainSocket.h"

#define UIBC_INPUT_TYPE_MOUSE_DOWN	0
#define UIBC_INPUT_TYPE_MOUSE_UP	1
#define UIBC_INPUT_TYPE_MOUSE_MOVE	2
#define UIBC_INPUT_TYPE_KEY_DOWN	3
#define UIBC_INPUT_TYPE_KEY_UP		4
#define UIBC_INPUT_TYPE_ZOOM		5
#define UIBC_INPUT_TYPE_VSCROLL		6
#define UIBC_INPUT_TYPE_HSCROLL		7
#define UIBC_INPUT_TYPE_ROTATE		8

SSideSyncMainSocket::SSideSyncMainSocket()
{
	_sid = 0;
}

SSideSyncMainSocket::~SSideSyncMainSocket()
{
}

SideSyncSocketError SSideSyncMainSocket::Initialize( int port, BOOL isServer )
{
	return SSideSyncSocket::Initialize(port, isServer);
}

SideSyncSocketError SSideSyncMainSocket::Destroy()
{
	return SSideSyncSocket::Destroy();
}

SideSyncSocketError SSideSyncMainSocket::Connect( LPCTSTR ipAddr )
{
	return SSideSyncSocket::Connect(ipAddr);
}

SideSyncSocketError SSideSyncMainSocket::Disconnect()
{
	return SSideSyncSocket::Disconnect();
}

//SideSyncSocketError SSideSyncMainSocket::SendRequest( int deviceId, int sid, byte* msg, int msg_len )
//{
//	return SSideSyncSocket::SendRequest(deviceId, sid, msg, msg_len);
//}
//
//SideSyncSocketError SSideSyncMainSocket::SendResponse( int deviceId, int sid, byte* msg, int msg_len )
//{
//	return SSideSyncSocket::SendResponse(deviceId, sid, msg, msg_len);
//}
//
//SideSyncSocketError SSideSyncMainSocket::SendPacket( byte* data, int data_len )
//{
//	return SSideSyncSocket::SendPacket(data, data_len);
//}

SideSyncSocketError SSideSyncMainSocket::ResetSID()
{
	_sid = 0;

	return OK;
}

SSendPacket* SSideSyncMainSocket::CreateCustomPacket( int request_id, char* data, c_uint64 len, bool isRequest )
{
	char* packet = new char[len + 8];
	packet[0] = (len >> 24) & 0xFF;
	packet[1] = (len >> 16) & 0xFF;
	packet[2] = (len >> 8) & 0xFF;
	packet[3] = len & 0xFF;
	packet[4] = (request_id >> 24) & 0xFF;
	packet[5] = (request_id >> 16) & 0xFF;
	packet[6] = (request_id >> 8) & 0xFF;
	packet[7] = request_id & 0xFF;
	memcpy(&packet[8], data, len);

	SSendPacket* sendPacket = new SSendPacket(packet, len + 8);
	delete[] packet;
	return sendPacket;
}

SideSyncSocketError SSideSyncMainSocket::SendRequestPacket( byte* data, int data_len )
{
	return SendRequest(0, _sid++, data, data_len);
}

void SSideSyncMainSocket::onReceiveListener( int socketid, char* ipaddr, char* data, int data_len )
{
	LOGD("onReceiveListener() is called");

	if (_socketEventListener != NULL)
		_socketEventListener->onReceiveRequest(socketid, &data[8], data_len - 8);
}

c_uint32 SSideSyncMainSocket::onRequestPacketSizeFromHeader( int headerSize, char* header )
{
	if (headerSize == 8)
	{
		c_uint32 packetSize = 0;
		packetSize += header[0];
		packetSize = packetSize << 8;
		packetSize += header[1];
		packetSize = packetSize << 8;
		packetSize += header[2];
		packetSize = packetSize << 8;
		packetSize += header[3];

		packetSize += headerSize;
		return packetSize;
	}
	return 0;
}

int SSideSyncMainSocket::onRequestHeaderSize()
{
	return 8;
}
