#include "stdafx.h"

#include "FileLog.h"
#include "common.h"

bool FileLog::bIsLogListenerSet = false;

FileLog mInstance;

FileLog::FileLog()
{
	_senderThread = -1;
	//mSideSyncLogListener = NULL;
}

void* FileLog::senderThreadMain(void* arg)
{
	FileLog* fLog = reinterpret_cast<FileLog*>(arg);
	fLog->senderThreadFunc();
	return 0;
}

int FileLog::senderThreadFunc(void)
{
	MSG msg;
	while(bIsLogListenerSet)
	{
		if(GetMessage(&msg, NULL, WM_SIDESYNC_LOG_PRINT_MESSAGE, WM_SIDESYNC_LOG_EXIT_MESSAGE))
		{
			switch(msg.message)
			{
			case WM_SIDESYNC_LOG_PRINT_MESSAGE:
				{
					wchar_t* pUnicodeStr = (wchar_t*)msg.wParam;
					CString logStr(pUnicodeStr);
					delete [] pUnicodeStr;
					//if (mInstance.mSideSyncLogListener != NULL)
					//	mInstance.mSideSyncLogListener->onPrintLog(logStr);
				}
				break;
			case WM_SIDESYNC_LOG_EXIT_MESSAGE:
				bIsLogListenerSet = false;
				break;
			default:
				break;
			}
		}
	}

	return 0;
}

//void FileLog::setLogListener(ILogEventListener* pListener)
//{	
//	mInstance.mSideSyncLogListener = pListener;
//	if(pListener != NULL)
//	{
//		bIsLogListenerSet = true;
//		mInstance._threadHandle = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)senderThreadMain, (void*) &mInstance, 0, &(mInstance._senderThread));
//	}
//	else
//	{
//		if(bIsLogListenerSet)
//		{
//			PostThreadMessage(mInstance._senderThread, WM_SIDESYNC_LOG_EXIT_MESSAGE, 0, 0);	
//			if(mInstance._senderThread >= 0)
//				WaitForSingleObject(mInstance._threadHandle, 1000L);
//			mInstance._senderThread = -1;
//		}
//	}
//}

void FileLog::Print(const char *pStr)
{
	//if (mInstance.mSideSyncLogListener == NULL) 
	//	return;
	wchar_t* pUnicodeStr = utf8_to_unicode_n(pStr);
	PostThreadMessage(mInstance._senderThread, WM_SIDESYNC_LOG_PRINT_MESSAGE, (WPARAM)pUnicodeStr, 0);
}
