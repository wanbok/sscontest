#include "StdAfx.h"
#include "SSideSyncSocketManager.h"
#include "NotifyFromNative.h"
#include "Settings.h"
#include "SendThreadLooper.h"
#include "SocketPacketFactory.h"

SSideSyncSocketManager::SSideSyncSocketManager(void)
{
	_socketMode = SSM_CLIENT;
	_sockMgr = NULL;

	_msgBuf = NULL;
	_commonMsgId = -1;
	_isReceivingCommonMsg = FALSE;
	_receivedCommonMsgLen = 0;

	_isConnecting = FALSE;

	_msgHandler = new MessageHandler(0, this);
}

SSideSyncSocketManager::~SSideSyncSocketManager(void)
{
	Destroy();

	SAFE_DELETE(_msgHandler);

	if (_msgBuf != NULL)
		delete[] _msgBuf;
}

SideSyncSocketError SSideSyncSocketManager::Initialize( int port, BOOL isServer )
{
	_socketMode = isServer == TRUE ? SSM_SERVER : SSM_CLIENT;

	if (_sockMgr != NULL)
		return ERROR_SOCKET_ALREADY_CREATED;

	_sockMgr = new SocketManager(port, this, EVENT_SOCKET_ID, isServer == TRUE ? true : false);
	CREATE_SEMAPHORE(_semaConfirmConnect);
	CREATE_SEMAPHORE(_semaDestroy);

	return OK;
}

SideSyncSocketError SSideSyncSocketManager::Destroy()
{
	CleanUp();

	return OK;
}

SideSyncSocketError SSideSyncSocketManager::Connect( LPCTSTR ipAddr )
{
	if (_sockMgr == NULL)
	{
		LOGE("Call initialize function first");
		return ERROR_NOT_INITIALIZED;
	}

	if (_sockMgr->isConnected())
	{
		LOGE("connect() - ERROR_SOCKET_ALREADY_CREATED");
		return ERROR_SOCKET_ALREADY_CREATED;
	}

	if (ipAddr == NULL || _tcslen(ipAddr) == 0)
	{
		LOGE("Input IP address first.");
		return ERROR_UNKNOWN;
	}

#ifdef UNICODE
	CStringA ipAddrA(ipAddr);
	char* connectIP = copyStr(ipAddrA);
#else
	char* connectIP = copyStr(ipAddr);
#endif

	LOGD("Connecting with Server IP %s", connectIP);

	int result = OK;

	_isConnecting = TRUE;

	int err = _sockMgr->createSocket(connectIP);
	delete[] connectIP;

	if (err != OK)
	{
		LOGE("start_server() failed, err : %d", err);		
		return _socketMode == SSM_SERVER ? ERROR_CANNOT_START_SERVER : ERROR_CANNOT_CONNECT_TO_SERVER;
	}

	LOGD("_sockMgr->createSocket() succeeded.");

	return OK;
}

SideSyncSocketError SSideSyncSocketManager::Disconnect()
{
	if (_sockMgr == NULL)
	{
		LOGE("Call initialize function first");
		return ERROR_NOT_INITIALIZED;
	}

	LOGD("Disconnect Called by Application");

	_isConnecting = FALSE;

	bool connected = _sockMgr->isConnected();
	_sockMgr->closeSocket();

	if (connected)
		_msgHandler->postMessage(new EventMessage(SIDE_SYNC_DISCONNECTED, _sockMgr->getConnectionType(), FROM_EVENT_SOCKET_CONTROLLER, _sockMgr->getConnectedIpAddr()));
	else
		NotifyFromNative::Notify(SIDE_SYNC_INTERNAL_ERROR, ERROR_SOCKET_ALREADY_DISCONNECTED);

	LOGD("Close connection");
	return OK;
}

void SSideSyncSocketManager::CleanUp()
{
	RELEASE_SEMAPHORE(_semaConfirmConnect);

	if (_sockMgr != NULL)
	{
		Disconnect();

		if (_sockMgr->isReceiveThreadRunning())
		{
			LOGD("<<WAIT DESTROY SEMAPHORE>> - IN");

			WAIT_SEMAPHORE_TIMEOUT(_semaDestroy, 3000L);

			LOGD("<<WAIT DESTROY SEMAPHORE>> - OUT");
		}

		while (_sockMgr->getConfirmState())
		{
			PAL_SLEEP_MS(10);
		}

		SAFE_DELETE(_sockMgr);
	}

	DESTROY_SEMAPHORE(_semaConfirmConnect);
	DESTROY_SEMAPHORE(_semaDestroy);
}

void SSideSyncSocketManager::onInternalEventListener( int eventID, int arg1, c_uint64 arg2, char* msg )
{
	switch (eventID)
	{
	case SIDE_SYNC_CONNECTED:
		LOGD("onInternalEventListener() - SIDE_SYNC_CONNECTED");

		Settings::getInstance()->setIsConnected(true);
		NotifyFromNative::Notify(SIDE_SYNC_CONNECTED, arg1, -1, msg);
		break;
	case SIDE_SYNC_DISCONNECTED:
		LOGD("onInternalEventListener() - SIDE_SYNC_DISCONNECTED - __isConnected = %s - from:%d",
			Settings::getInstance()->isConnected() ? "True" : "False", arg2);

		if (Settings::getInstance()->isConnected())
		{
			Settings::getInstance()->setIsConnected(false);

			if (_socketMode == SSM_CLIENT)
			{
				switch (arg2)
				{
				case FROM_EVENT_SOCKET_CONTROLLER:
					break;
				case FROM_FILE_SOCKET_CONTROLLER:
					Disconnect();
					break;
				default:
					break;
				}
			}
			NotifyFromNative::Notify(SIDE_SYNC_DISCONNECTED, arg1, -1, msg);
		}
		break;
	default:
		LOGD("onInternalEventListener() - Not currently supported event (eventID : %d)", eventID);
		break;
	}
}

SideSyncSocketError SSideSyncSocketManager::sendCommonRequest( int requestId, const char* msg, int msg_len )
{
	if (_sockMgr == NULL)
	{
		LOGE("Call initialize function first");
		return ERROR_NOT_INITIALIZED;
	}

	//if (!_sockMgr->isConnected())
	//{
	//	LOGE("_sockMgr::cancel_file_sending - No client connected");
	//	return ERROR_NO_CLIENT_CONNECTED;
	//}

	char* msgcpy = new char[msg_len];
	memcpy(msgcpy, msg, msg_len);

	SendTask * task = new SendTask();
	task->task_type = TASK_TYPE_COMMON_REQUEST;
	task->setObj(msgcpy, msg_len);
	task->arg1 = requestId;

	int _taskID = _sockMgr->getSendLooper()->submitTask(task);

	delete task;
	delete[] msgcpy;

	return OK;
}

SideSyncSocketError SSideSyncSocketManager::sendCommonResponse( int requestId, const char* msg, int msg_len )
{
	if (_sockMgr == NULL)
	{
		LOGE("Call initialize function first");
		return ERROR_NOT_INITIALIZED;
	}

	//if (!_sockMgr->isConnected())
	//{
	//	LOGE("_sockMgr::cancel_file_sending - No client connected");
	//	return ERROR_NO_CLIENT_CONNECTED;
	//}

	char* msgcpy = new char[msg_len];
	memcpy(msgcpy, msg, msg_len);

	SendTask * task = new SendTask();
	task->task_type = TASK_TYPE_COMMON_RESPONSE;
	task->setObj(msgcpy, msg_len);
	task->arg1 = requestId;

	int _taskID = _sockMgr->getSendLooper()->submitTask(task);

	delete task;
	delete[] msgcpy;

	return OK;
}

void SSideSyncSocketManager::onAcceptListener( int socketid, char* ipaddr )
{
	LOGD("on_accept_listener - socketid = %d ip=%s", socketid, ipaddr);
}

void SSideSyncSocketManager::onConnectedListener( int socketid, char* ipaddr )
{
	LOGD("on_connected_listener - socketid = %d ip=%s", socketid, ipaddr);

	if (_sockMgr == NULL)
	{
		LOGE("on_connected_listener() - mSocketManager == NULL");
		return;
	}

	_sockMgr->setConnected(true);

	return;


	//Send confirm with server
	_sockMgr->setConfirmState(true);

	if (!_sockMgr->needSecurity())
	{
		//Send confirm with server
		LOGD("on_connected_listener - USB connection");

		SendConfirmConnected();
	}
	else
	{
		//Send security hello with server
		LOGD("on_connected_listener - Wi-Fi connection");

		// TODO: 아래 Code는 아직 Security를 지원하지 않아, 주석처리합니다.
		//       Security 지원시 필요할 경우 해당 Code를 구현하세요.

		//int pwdLength = 0;
		//char* password = __socketManager->getAuthenticationInfo(pwdLength);
		//if (password != NULL) sendSecurityHello(password, pwdLength);
	}

	if (_sockMgr->isConnectedViaWiFi())
	{
		// Wait till CONNECT_TIMEOUT_WIFI with sleep interval of CONNECT_TIMEOUT_INTERVAL
		//for (int i = 0; i * CONNECT_TIMEOUT_INTERVAL < CONNECT_TIMEOUT_WIFI; i++)
		//usleep(CONNECT_TIMEOUT_INTERVAL * MILLI_UNIT);
		WAIT_SEMAPHORE_TIMEOUT(_semaConfirmConnect, CONNECT_TIMEOUT_WIFI);
	}
	else
	{
		// Sleep for CONNECT_TIMEOUT_USB
		//usleep(CONNECT_TIMEOUT_USB * MILLI_UNIT);
		WAIT_SEMAPHORE_TIMEOUT(_semaConfirmConnect, CONNECT_TIMEOUT_USB);
	}

	if (_sockMgr != NULL)
	{
		// check connected
		if ((_sockMgr->isConnected() == false) && (_isConnecting == TRUE))
		{
			LOGE("on_connected_listener, mSocketManager->isConnected() == false !!!");

			onConnectionFailedListener(socketid, ipaddr);

			Disconnect();
		}
		else
		{
			LOGD("on_connected_listener, mSocketManager->isConnected() == true !!!");

			if (_sockMgr->isConnectedViaWiFi())
			{

			}
		}

		_sockMgr->setConfirmState(false);
	}
}

void SSideSyncSocketManager::onConnectionFailedListener( int socketid, char* ipaddr )
{
	LOGE("onConnectionFailedListener() is called");

	if (_sockMgr != NULL)
		NotifyFromNative::Notify(SIDE_SYNC_CONNECT_FAILED, _sockMgr->getConnectionType(), -1, ipaddr);
}

void SSideSyncSocketManager::onReceiveListener( int socketid, char* ipaddr, char* data, int data_len )
{
	int err = OK;

	PacketParser *packet_parser = new PacketParser(data);

	switch (packet_parser->getType())
	{
	case CONFIRM_CONNECTED:
		Settings::getInstance()->setConnectedProtocolVer(packet_parser->getVersion());

		LOGD("CONFIRM_CONNECTED - My Version: %d - Connected Version: %d", Settings::getInstance()->getMyVersion(), Settings::getInstance()->getConnectedProtocolVer());

		if (!_sockMgr->needSecurity())
		{
			LOGD("on_receive_listener - USB connection");
		}
		else
		{
			LOGD("on_receive_listener - Wi-Fi connection. need security setup.");
			//@TODO
			// Don't accept connection without security setup when connected via Wi-Fi.
		}

		//Server
		if (_sockMgr->isServer())
			SendConfirmConnected();

		_sockMgr->setConnected(true);

		_msgHandler->postMessage(new EventMessage(SIDE_SYNC_CONNECTED, _sockMgr->getConnectionType(), -1, ipaddr));
		break;

	//case CONTROL_KEYBOARD_EVENT:
	//	receiveKeyboardEvent(packet_parser);
	//	break;

	//case CONTROL_MOUSE_EVENT:
	//	receiveMouseEvent(packet_parser);
	//	break;

	//	//Receiver cancel file receiving
	//case FILE_TRANSMISSION_RECV_CANCELED:
	//	break;

	//	//Sender cancel file sending
	//case FILE_TRANSMISSION_SEND_CANCELED: //
	//	break;

	//case CHANGE_WORKING_DEVICE_NOTIFICATION:
	//	if (packet_parser->getDataLength() > 0)
	//	{
	//		int change = packet_parser->getDataBuffer()[0];
	//		NotifyFromNative::Notify(SIDE_SYNC_CHANGE_WORKING_DEVICE, change);
	//	}
	//	else
	//	{
	//		LOGE("CHANGE_WORKING_DEVICE_NOTIFICATION - packet is broken");
	//	}
	//	break;

	//case DISPLAY_RESOLUTION_REQUEST:
	//	NotifyFromNative::Notify(SIDE_SYNC_RECEIVE_DISPLAY_RESOLUTION_REQUEST);
	//	break;

	//case DISPLAY_RESOLUTION:
	//	NotifyFromNative::Notify(SIDE_SYNC_RECEIVE_DISPLAY_RESOLUTION_RESPONSE, -1, -1, packet_parser->getDataBuffer(), packet_parser->getDataLength());
	//	break;

	//case DEVICE_NAME_REQUEST:
	//	NotifyFromNative::Notify(SIDE_SYNC_RECEIVE_DEVICE_NAME_REQUEST);
	//	break;

	//case DEVICE_NAME:
	//	{
	//		int device_name_len = packet_parser->getDataLength();
	//		unsigned char* device_name = new unsigned char[device_name_len + 1];
	//		for (int i = 0; i < device_name_len; i++)
	//			device_name[i] = packet_parser->getDataBuffer()[i];
	//		device_name[device_name_len] = NULL;

	//		NotifyFromNative::Notify(SIDE_SYNC_RECEIVE_DEVICE_NAME_RESPONSE, -1, -1, device_name, device_name_len + 1);

	//		delete [] device_name;
	//	}
	//	break;

	case DATA_COMMON_REQUEST_START:
	case DATA_COMMON_REQUEST_BUFFER:
	case DATA_COMMON_REQUEST_END:
		receiveCommonMessage(packet_parser, true);
		break;

	case DATA_COMMON_RESPONSE_START:
	case DATA_COMMON_RESPONSE_BUFFER:
	case DATA_COMMON_RESPONSE_END:
		receiveCommonMessage(packet_parser, false);
		break;

	//case DATA_CLIPBOARD_START:
	//case DATA_CLIPBOARD_BUFFER:
	//case DATA_CLIPBOARD_END:
	//	receiveClipboard(packet_parser);
	//	break;

	//case SECURITY_KEY_EXCHANGE:
	//	break;

	//case SECURITY_DATA:
	//	break;

	case HEARTBEAT:
		{
			//LOGD("HeartBeat from client is received");
			sendHeartBeatResponse();
			break;
		}

	case HEARTBEAT_RESPONSE:
		{
			//LOGD("HeartBeat response from server is received");
			break;
		}

	//case FILE_TRANSMISSION_RECVER_READY:
	//	{
	//		//LOGD("onReceiveListener() - FILE_TRANSMISSION_RECVER_READY");
	//		break;
	//	}
	default:
		break;

	}
	delete (packet_parser);
}

void SSideSyncSocketManager::onClientCloseListener( int socketid, char* ipaddr )
{
	LOGE("onClientCloseListener() is called");

	_isConnecting = FALSE;

	if (_sockMgr != NULL)
		_msgHandler->postMessage(new EventMessage(SIDE_SYNC_DISCONNECTED, _sockMgr->getConnectionType(),
			FROM_EVENT_SOCKET_CONTROLLER, ipaddr));
}

void SSideSyncSocketManager::onSocketDestroy()
{
	LOGD("onSocketDestroy() is called");

	RELEASE_SEMAPHORE(_semaDestroy);
}

int SSideSyncSocketManager::sendHeartBeatResponse()
{
	if (_sockMgr == NULL)
	{
		LOGE("sendHeartBeatResponse() - mSocketManager == NULL");
		return ERROR_SOCKET_MANAGER_NULL;
	}

	if (_sockMgr->isConnected())
	{
		LOGE("SSideSyncMainCommManager::sendHeartBeatResponse - No client connected");
		return ERROR_NO_CLIENT_CONNECTED;
	}

	int packet_len = 0;
	char* heartbeatResponsePacket = SocketPacketFactory::getHeartBeatResponsePacket(&packet_len);
	SendTask * task = new SendTask();
	task->task_type = TASK_TYPE_PACKET;
	task->setObj(heartbeatResponsePacket, packet_len);

	_sockMgr->getSendLooper()->submitTask(task);

	delete[] heartbeatResponsePacket;
	delete task;

	return OK;
}

int SSideSyncSocketManager::receiveCommonMessage( PacketParser* packetParser, bool isRequest )
{
	try
	{
		switch (packetParser->getType())
		{
		case DATA_COMMON_REQUEST_START:
		case DATA_COMMON_RESPONSE_START:
			{
				_isReceivingCommonMsg = TRUE;
				_receivedCommonMsgLen = 0;
				_commonMsgId = byte2Int(packetParser->getDataBuffer(), 0, true);
				if (_msgBuf != NULL)
				{
					delete[] _msgBuf;
					_msgBuf = NULL;
				}

				int dataSize = packetParser->getDataLength();
				char* pByteBuffer = packetParser->getDataBuffer();

				if (dataSize <= 0 || pByteBuffer == NULL)
				{
					LOGD("Clipboard buffer is empty ");
					return ERROR_RECEIVED_DATA_NULL;
				}

				//Get clipboard length
				c_uint64 messageLength = byte2Long(pByteBuffer, SIZE_OF_INT, true);

				LOGD("DATA_COMMON_MESSAGE_START length=%lld", messageLength);

				if (messageLength == 0)
				{
					LOGD("Clipboard buffer is empty");
					return ERROR_RECEIVED_CLIPBOARD_EMPTY;
				}
				_msgBuf = new char[messageLength];
			}
			break;

		case DATA_COMMON_REQUEST_BUFFER:
		case DATA_COMMON_RESPONSE_BUFFER:
			LOGD("DATA_COMMON_REQUEST(RESPONSE)_BUFFER ");

			if (_isReceivingCommonMsg == FALSE)
			{
				LOGD("Start clipboard packet was not received");
				return ERROR_IS_NOT_CLIPBOARD_RECEIVE_STATUS;
			}

			memcpy(_msgBuf + _receivedCommonMsgLen, packetParser->getDataBuffer(),
				packetParser->getDataLength()); //SIZE_OF_INT == sent data length (redundancy)
			_receivedCommonMsgLen += packetParser->getDataLength();
			break;

		case DATA_COMMON_REQUEST_END:
		case DATA_COMMON_RESPONSE_END:
			LOGD("DATA_COMMON_REQUEST(RESPONSE)_END ");

			if (_isReceivingCommonMsg == FALSE)
			{
				LOGD("Start clipboard packet was not received");
				return ERROR_IS_NOT_CLIPBOARD_RECEIVE_STATUS;
			}
			_isReceivingCommonMsg = FALSE;

			LOGD("Common message received successfully - length: %lld", _receivedCommonMsgLen);

			NotifyFromNative::Notify((isRequest ? SIDE_SYNC_RECEIVE_COMMON_REQUEST : SIDE_SYNC_RECEIVE_COMMON_RESPONSE),
				_commonMsgId, _receivedCommonMsgLen, _msgBuf, _receivedCommonMsgLen, BYTE_PTR);

			delete[] _msgBuf;
			_msgBuf = NULL;
			break;
		}
	}
	catch (exception& e)
	{
		LOGE("EventSocketController::receive_common_message - Error - %s", e.what());
	}

	return OK;
}

int SSideSyncSocketManager::SendConfirmConnected()
{
	if (_sockMgr == NULL)
	{
		LOGE("SendConfirmConnected() - mSocketManager == NULL");
		return ERROR_SOCKET_MANAGER_NULL;
	}

	if (!_sockMgr->isSocketConnected())
	{
		LOGE("SendConfirmConnected - No client connected");
		return ERROR_NO_CLIENT_CONNECTED;
	}

	LOGD("Server:: submit task to send confirm connect start");

	int packet_len = 0;
	char* confirm_packet = SocketPacketFactory::connectConfimed(&packet_len);
	SendTask *task = new SendTask();
	task->task_type = TASK_TYPE_PACKET;
	task->setObj(confirm_packet, packet_len);

	LOGD("Server:: submit task to send confirm connect end");

	int _taskID = _sockMgr->getSendLooper()->submitTask(task);

	delete[] confirm_packet;
	delete task;

	return OK;
}
