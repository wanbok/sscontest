#include "stdafx.h"

#include "NotifyFromNative.h"

#include <exception>
#include "log.h"
#include "common.h"
#include "MessageType.h"
#define LOG_TAG "NotifyFromNative"

bool NotifyFromNative::bIsEventListenerSet = false;

NotifyFromNative mInstance;

NotifyFromNative::NotifyFromNative()
{
	_senderThread = -1;
	mSideSyncEventListener = NULL;
}

void* NotifyFromNative::senderThreadMain(void* arg)
{
	NotifyFromNative* mPtr = reinterpret_cast<NotifyFromNative*>(arg);
	MSG msg;
	while(bIsEventListenerSet)
	{
		if(GetMessage(&msg, NULL, WM_SIDESYNC_NOTIFY_SEND_MESSAGE, WM_SIDESYNC_NOTIFY_EXIT_MESSAGE))
		{
			switch(msg.message)
			{
			case WM_SIDESYNC_NOTIFY_SEND_MESSAGE:
				{
					NotifyParam* nparam = (NotifyParam*)msg.wParam;
					if (mInstance.mSideSyncEventListener != NULL)
						mInstance.notify(nparam->event_id, nparam->arg1, nparam->arg2, nparam->data, nparam->data_len, nparam->data_type, nparam->data_extended);

					if(nparam->data_len > 0)
						delete [] nparam->data;
					delete nparam;
				}
				break;
			case WM_SIDESYNC_NOTIFY_EXIT_MESSAGE:
				bIsEventListenerSet = false;
				break;
			default:
				break;
			}
		}
	}
	return 0;
}

void NotifyFromNative::setListener(ISideSyncSocketEventListener* pListener)
{
	mInstance.mSideSyncEventListener = pListener;
	if(pListener != NULL)
	{
		bIsEventListenerSet = true;
		mInstance._threadHandle = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)senderThreadMain, (void*) &mInstance, 0, &(mInstance._senderThread));
	}
	else
	{
		if(bIsEventListenerSet)
		{
			PostThreadMessage(mInstance._senderThread, WM_SIDESYNC_NOTIFY_EXIT_MESSAGE, 0, 0);
			if(mInstance._senderThread >= 0)
				WaitForSingleObject(mInstance._threadHandle, 1000L);
			mInstance._senderThread = -1;
		}
	}
}

void NotifyFromNative::Notify(int eventID, int arg1, int arg2, const void* data,int data_len, int dataType, char* data_extended)
{
	char* pData = (char*)data;
	char* extendedData = data_extended;

	int tempLen = 0;

	if(data != NULL)
	{
		if(data_len > 0)
			tempLen = data_len;
		else if((*pData) != NULL)
			tempLen = strlen(pData);

		if(tempLen > 0)
		{
			pData = new char[tempLen+1];
			memset(pData, 0, tempLen+1);
			memcpy(pData, data, tempLen);
		}

		//for extended data
		if(data_extended != NULL)
		{
			int __len = strlen(data_extended);

			if(__len > 0)
			{
				extendedData = new char[__len+1];
				memset(extendedData, 0, __len+1);
				memcpy(extendedData, data_extended, __len);
			}
		}
	}

	NotifyParam* nparam = new NotifyParam();
	nparam->event_id = eventID;
	nparam->arg1 = arg1;
	nparam->arg2 = arg2;
	nparam->data = pData;
	nparam->data_len = tempLen;
	nparam->data_type = dataType;
	nparam->data_extended = extendedData;

	PostThreadMessage(mInstance._senderThread, WM_SIDESYNC_NOTIFY_SEND_MESSAGE, (WPARAM)nparam, 0);
}

#ifdef UNICODE
LPWSTR GetUnicodeString(const void* data, int length = -1)
{
	LPWSTR retVal = NULL;

	if (length == -1)
	{
		if (data == NULL)
		{
			retVal = new WCHAR[1];
			retVal[0] = L'\0';
			return retVal;
		}
	}
	else
	{
		if (length <= 0 || data == NULL)
		{
			retVal = new WCHAR[1];
			retVal[0] = L'\0';
			return retVal;
		}
	}

	int size = MultiByteToWideChar(CP_UTF8, 0, (LPCSTR)data, length, NULL, 0);

	retVal = new WCHAR[size];

	if (MultiByteToWideChar(CP_UTF8, 0, (LPCSTR)data, length, retVal, size) == 0)
	{
		LOGE("GetUnicodeString() : MultiByteToWideChar() returns error. (GetLastError() returns 0x%08X.)", GetLastError());
	}

	return retVal;
}
#endif

void NotifyFromNative::notify(int eventID, int arg1, int arg2, const void* data,int data_len, int dataType, char* data_extended)
{
#ifdef UNICODE
	LPWSTR unicodeStr = NULL;
#endif
	switch(eventID)
	{

	case SIDE_SYNC_CONNECTED:
		{
#ifdef UNICODE
			unicodeStr = GetUnicodeString(data);
			if(mSideSyncEventListener != NULL)
				mSideSyncEventListener->onConnected(arg1, unicodeStr);
			delete[] unicodeStr;
#else
			if(mSideSyncEventListener != NULL)
				mSideSyncEventListener->onConnected(arg1, data!=NULL?(LPCTSTR)data:NULL);
#endif
		}
		break;
	case SIDE_SYNC_DISCONNECTED:
		{
#ifdef UNICODE
			unicodeStr = GetUnicodeString(data);
			if(mSideSyncEventListener != NULL)
				mSideSyncEventListener->onDisconnected(arg1, unicodeStr);
			delete[] unicodeStr;
#else
			if(mSideSyncEventListener != NULL)
				mSideSyncEventListener->onDisconnected(arg1, data!=NULL?(LPCTSTR)data:NULL);
#endif
		}
		break;
	case SIDE_SYNC_CONNECT_FAILED:
		{
#ifdef UNICODE
			unicodeStr = GetUnicodeString(data);
			if(mSideSyncEventListener != NULL)
				mSideSyncEventListener->onConnectFailed(arg1, unicodeStr);
			delete[] unicodeStr;
#else
			if(mSideSyncEventListener != NULL)
				mSideSyncEventListener->onConnectFailed(arg1, data!=NULL?(LPCTSTR)data:NULL);
#endif
		}
		break;
		//[20130627] Canh added
	case SIDE_SYNC_CONNECT_LAGGED:
		{
#ifdef UNICODE
			unicodeStr = GetUnicodeString(data);
			if(mSideSyncEventListener != NULL)
				mSideSyncEventListener->onConnectLagged(arg1, unicodeStr);
			delete[] unicodeStr;
#else
			if(mSideSyncEventListener != NULL)
				mSideSyncEventListener->onConnectLagged(arg1, data!=NULL?(LPCTSTR)data:NULL);
#endif
		}
		break;
	case SIDE_SYNC_RECEIVE_CLIPBOARD_DATA:
		{
			// arg1 : length
			// data : clipboard data
			if(mSideSyncEventListener != NULL)
				mSideSyncEventListener->onReceiveClipBoard(data!=NULL?(char*)data:NULL, arg1);
		}
		break;
	case SIDE_SYNC_FILE_RECEIVE_START:
		{
			c_uint64 fileSize = mergeTo64(arg1, arg2);

			wchar_t* pUnicodeName = utf8_to_unicode_n((const char*)data);
			if (pUnicodeName == NULL)
			{
				LOGE("cannot convert string from utf8 to unicode");
				break;
			}

			CString fileName(pUnicodeName);

			wchar_t* pModifiedName = utf8_to_unicode_n((const char*)data_extended);
			CString modifiedName(pModifiedName);

			if(mSideSyncEventListener != NULL)
				mSideSyncEventListener->onReceiveFileTransmissionStart(fileName, modifiedName, fileSize);

			delete[] pUnicodeName;
			delete[] data_extended;
		}
		break;
	case SIDE_SYNC_FILE_RECEIVE_STATUS:
		{
			c_uint64 sentBytes = mergeTo64(arg1, arg2);

			wchar_t* pUnicodeName = utf8_to_unicode_n((const char*)data);
			if (pUnicodeName == NULL)
			{
				LOGE("cannot convert string from utf8 to unicode");
				break;
			}

			CString fileName(pUnicodeName);

			wchar_t* pModifiedName = utf8_to_unicode_n((const char*)data_extended);
			CString modifiedName(pModifiedName);

			if(mSideSyncEventListener != NULL)
				mSideSyncEventListener->onReceiveFileTransmissionStatus(fileName, modifiedName, sentBytes);

			delete[] pUnicodeName;
			delete[] data_extended;
		}
		break;
	case SIDE_SYNC_FILE_RECEIVE_SUCCESS:
		{
			c_uint64 fileSize = mergeTo64(arg1, arg2);

			wchar_t* pUnicodeName = utf8_to_unicode_n((const char*)data);
			if (pUnicodeName == NULL)
			{
				LOGE("cannot convert string from utf8 to unicode");
				break;
			}

			CString fileName(pUnicodeName);

			wchar_t* pModifiedName = utf8_to_unicode_n((const char*)data_extended);
			CString modifiedName(pModifiedName);

			if(mSideSyncEventListener != NULL)
				mSideSyncEventListener->onReceiveFileTransmissionSuccess(fileName, pModifiedName, fileSize);

			delete[] pUnicodeName;
			delete[] data_extended;
		}
		break;

	case SIDE_SYNC_FILE_TRANSMISSION_RECEIVER_CANCELED:
		{
			wchar_t* pUnicodeName = utf8_to_unicode_n((const char*)data);
			if (pUnicodeName == NULL)
			{
				LOGE("cannot convert string from utf8 to unicode");
				break;
			}

			CString fileName(pUnicodeName);
			if(mSideSyncEventListener != NULL)
				mSideSyncEventListener->onSendFileTransmissionCanceled(fileName);

			delete[] pUnicodeName;
		}
		break;

	case SIDE_SYNC_FILE_RECEIVE_CANCELED:
		{
			wchar_t* pUnicodeName = utf8_to_unicode_n((const char*)data);
			if (pUnicodeName == NULL)
			{
				LOGE("cannot convert string from utf8 to unicode");
				break;
			}

			CString fileName(pUnicodeName);
			if(mSideSyncEventListener != NULL)
				mSideSyncEventListener->onSendFileTransmissionCanceled(fileName);

			delete[] pUnicodeName;
		}
		break;

	case SIDE_SYNC_FILE_RECEIVE_FAILED:
		{
			if(mSideSyncEventListener != NULL)
				mSideSyncEventListener->onReceivingFileTransmissionFailed();
		}
		break;

	case SIDE_SYNC_FILE_SEND_START:
		{
			c_uint64 fileSize = mergeTo64(arg1, arg2);

			wchar_t* pUnicodeName = utf8_to_unicode_n((const char*)data);
			if (pUnicodeName == NULL)
			{
				LOGE("cannot convert string from utf8 to unicode");
				break;
			}

			CString fileName(pUnicodeName);
			if(mSideSyncEventListener != NULL)
				mSideSyncEventListener->onSendFileTransmissionStart(fileName, fileSize);

			delete[] pUnicodeName;
		}
		break;
	case SIDE_SYNC_FILE_SEND_STATUS:
		{
			c_uint64 sentBytes = mergeTo64(arg1, arg2);

			wchar_t* pUnicodeName = utf8_to_unicode_n((const char*)data);
			if (pUnicodeName == NULL)
			{
				LOGE("cannot convert string from utf8 to unicode");
				break;
			}

			CString fileName(pUnicodeName);
			if(mSideSyncEventListener != NULL)
				mSideSyncEventListener->onSendFileTransmissionStatus(fileName, sentBytes);

			delete[] pUnicodeName;
		}
		break;
	case SIDE_SYNC_FILE_SEND_SUCCESS:
		{
			c_uint64 fileSize = mergeTo64(arg1, arg2);

			wchar_t* pUnicodeName = utf8_to_unicode_n((const char*)data);
			if (pUnicodeName == NULL)
			{
				LOGE("cannot convert string from utf8 to unicode");
				break;
			}

			CString fileName(pUnicodeName);
			if(mSideSyncEventListener != NULL)
				mSideSyncEventListener->onSendFileTransmissionSuccess(fileName, fileSize);

			delete[] pUnicodeName;
		}
		break;
	case SIDE_SYNC_FILE_SEND_CANCELED:
		{
			wchar_t* pUnicodeName = utf8_to_unicode_n((const char*)data);
			if (pUnicodeName == NULL)
			{
				LOGE("cannot convert string from utf8 to unicode");
				break;
			}

			CString fileName(pUnicodeName);
			if(mSideSyncEventListener != NULL)
				mSideSyncEventListener->onSendFileTransmissionCanceled(fileName);

			delete[] pUnicodeName;
		}
		break;

	case SIDE_SYNC_FILE_TRANSMISSION_SENDER_CANCELED:
		{
			wchar_t* pUnicodeName = utf8_to_unicode_n((const char*)data);
			if (pUnicodeName == NULL)
			{
				LOGE("cannot convert string from utf8 to unicode");
				break;
			}

			CString fileName(pUnicodeName);

			wchar_t* pModifiedName = utf8_to_unicode_n((const char*)data_extended);
			CString modifiedName(pModifiedName);

			if(mSideSyncEventListener != NULL)
				mSideSyncEventListener->onReceiveFileTransmissionCanceled(fileName, pModifiedName);

			delete[] pUnicodeName;
			delete[] data_extended;
		}
		break;

	case SIDE_SYNC_FILE_SEND_FAILED:
		{
			wchar_t* pUnicodeName = utf8_to_unicode_n((const char*)data);
			if (pUnicodeName == NULL)
			{
				LOGE("cannot convert string from utf8 to unicode");
				break;
			}

			CString fileName(pUnicodeName);


			if(mSideSyncEventListener != NULL)
				mSideSyncEventListener->onSendingFileTransmissionFailed(fileName, arg2);

			delete[] pUnicodeName;

		}
		break;

	case SIDE_SYNC_RECEIVE_KEYBOARD_EVENT:

		// PC doesn't receive this event
		break;

	case SIDE_SYNC_RECEIVE_MOUSE_EVENT:

		// PC doesn't receive this event
		break;

	case SIDE_SYNC_CHANGE_WORKING_DEVICE:

		// PC doesn't receive this event
		break;

	case SIDE_SYNC_RECEIVE_DISPLAY_RESOLUTION_RESPONSE:
		{
			char type = ((char*)data)[0];
			int width = byte2Int((char*)data, 1, true);
			int height = byte2Int((char*)data, 5, true);
			if(mSideSyncEventListener != NULL)
				mSideSyncEventListener->onReceiveDisplayResolution((int)type, width, height);
		}
		break;

	case SIDE_SYNC_RECEIVE_DEVICE_NAME_REQUEST:

		// PC doesn't receive this event
		break;

	case SIDE_SYNC_RECEIVE_DEVICE_NAME_RESPONSE:
		{

			wchar_t* pDeviceName = utf8_to_unicode_n((const char*)data);
			if (pDeviceName == NULL)
			{
				LOGE("cannot convert string from utf8 to unicode");
				break;
			}

			CString fileName(pDeviceName);

			if(mSideSyncEventListener != NULL)
				mSideSyncEventListener->onReceiveDeviceName(pDeviceName);

			delete[] pDeviceName;

		}
		break;

	case SIDE_SYNC_RECEIVE_COMMON_REQUEST:

		// arg1 : request id
		// arg2 : length
		if(mSideSyncEventListener != NULL)
			mSideSyncEventListener->onReceiveCommonRequest(arg1, (char*)data, arg2);
		break;

	case SIDE_SYNC_RECEIVE_COMMON_RESPONSE:

		// arg1 : request id
		// arg2 : length
		if(mSideSyncEventListener != NULL)
			mSideSyncEventListener->onReceiveCommonResponse(arg1, (char*)data, arg2);

		break;
	case SIDE_SYNC_INTERNAL_ERROR:
		if(mSideSyncEventListener != NULL)
			mSideSyncEventListener->onError(arg1,data!=NULL?(char*)data:NULL);
		break;

	case SIDE_SYNC_RECEIVE_FILE_DATA:
		{
			c_uint64 fileOffset = mergeTo64(arg1, arg2);

			wchar_t* pUnicodeName = utf8_to_unicode_n((const char*)data_extended);
			if (pUnicodeName == NULL)
			{
				LOGE("cannot convert string from utf8 to unicode");
				break;
			}

			CString fileName(pUnicodeName);

			if(mSideSyncEventListener != NULL)
				mSideSyncEventListener->onFileDataReceived(fileName,(char*) data, data_len, fileOffset);

			delete[] pUnicodeName;
			delete[] data_extended;
		}
		break;

	default:
		break;

	}
}
