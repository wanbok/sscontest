#pragma once

#include "Common.h"
#include "Log.h"
#include "time.h"

#define MAX_CLIPBOARD_SIZE 1024*2 //  2000 characters
#define MAX_FILE_DOWNLOADER 2
#define PROTOCOL_VERSION 3
#define MULTI_SOCK_PROTOCOL_VERSION 3

class Settings
{
public:
	virtual ~Settings();
	static Settings* getInstance();
	static void destroyInstance();
	const char* getReceiveFolder();
	void setReceiveFolder(const char* folder);
	int getMyVersion();
	int getConnectedProtocolVer();
	bool isConnected();
	void setIsConnected(bool);
	void setConnectedProtocolVer(int connectedVersion);
	c_uint64 getNetworkRTT();
	void setNetworkRTT(c_uint64 value);
	void setLastTime();
	time_t* getLastTime();
	bool bSlowDownFileTransferring;

private:
	Settings();
	char* __receiveFolder;
	int __myVersion;
	int __connectedVersion; //As default;
	bool __isConnected;
	static Settings* __instance;
	c_uint64 __networkRTT;
	time_t last_event;
};
