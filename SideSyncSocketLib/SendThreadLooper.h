#pragma once

#include <list>

#include "common.h"
#include "Log.h"

#include "MessageType.h"
#include "PalSemaphore.h"
#include "PalMutex.h"

using namespace std;

enum SocketDataType
{
	SocketData_Packet = 0,
	SocketData_Request,
	SocketData_Response
};

struct SendTask
{
private:
	char* obj;
	bool is_set_obj;
	c_uint64 obj_len;

public:
	int task_type;
	int task_id;
	c_uint64 arg1,arg2;
	SendTask();
	
	void setObj(const char *_obj,c_uint64 objSize);

	char* getObj();

	c_uint64 getObjLen();

	void destroy();

};

class SSendPacket
{
public:
	SSendPacket(char *packet, int packet_len);
	~SSendPacket();

	char* GetPacket() { return _packet; }
	int GetLength() { return _packet_len; }
private:
	char *_packet;
	int _packet_len;
};

class IPacketMaker
{
public:
	virtual SSendPacket* CreateCustomPacket(int request_id, char* data, c_uint64 len, bool isRequest) { return NULL; }
};

typedef list<SendTask> TASKLIST;

class SendThreadLooper
{
	public:
		SendThreadLooper(int instantID, bool isTCP = true);
		~SendThreadLooper();
		void SetPacketMaker(IPacketMaker *maker) { _packetMaker = maker; }

		void start(int sockid, bool security, char* desIPAddr = NULL, int desPort=-1);
		void stop();
		int submitTask(SendTask *task);
		int cancelTasks(bool userCanceled = true);
		void signalToSend();
		void setWaitToSend(bool needWaitToSend);
		void setSkipWaitToSendReadyRequest();
		static int taskCount;
		bool isRunning();

	protected:
		static void* sendThreadMain(void *arg);
		void sendThreadLoop(void);
		bool _isCancelled, _isUserCanceled;
		DECLARE_SEMAPHORE(__semap);

	private:
		int __instantID;
		int __pcClientFd;
		bool __isSendThreadAlive;
		pthread_t __pthreadId;
		//DECLARE_MUTEX;
		CRITICAL_SECTION cs;

		TASKLIST *__pTaskList;

		int send_packet(char* packet_buff, int len);
		struct addrinfo *__adrInfo;

		bool __isTCP;
		char* __desIPAddr;
		int __desPort;
		bool __needWaitToSend;
		bool __skipWaitToSendReadyRequest;
		PalSemaphore* __waitToSendSemap;
		PalMutex* __readyToSendMutex;

		IPacketMaker *_packetMaker;
};
