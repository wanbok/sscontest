#pragma once

#include "Common.h"
#include "Log.h"
#include <map>

using namespace std;

class PalMutex
{
public:
	PalMutex(const char* id, bool showLog = false);
	~PalMutex();
	void lock();
	void unlock();
private:
	DECLARE_MUTEX_V(__internalMutex);
	CRITICAL_SECTION cs;
	char* __id;
	map<c_uint32, bool> __lockThreadMap;
	bool __isShowLog;
};
