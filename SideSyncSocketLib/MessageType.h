#ifndef _MESSAGE_TYPE_H
#define _MESSAGE_TYPE_H
#include <exception>
#include "common.h"
class ISocketManagerListener
{
public:

	virtual void		onAcceptListener(int, char*) = 0;
	virtual void		onConnectedListener(int, char*) = 0;
	virtual void		onConnectionFailedListener(int, char*) = 0;
	virtual void		onReceiveListener(int, char*, char*, int) = 0;
	virtual void		onClientCloseListener(int, char*) = 0;
	virtual void		onSocketDestroy() = 0;
	virtual int			onRequestHeaderSize() = 0;
	virtual c_uint32	onRequestPacketSizeFromHeader(int headerSize, char* header) = 0;
};

class IInternalEventListener
{
public:

	virtual void onInternalEventListener(int,int, c_uint64, char*) = 0;
};

enum SEMAPHORE_ID
{
	SEMAP_RECEIVE_END_ID = 0,
	SEMAP_SEND_LOOPER_FILE_SYNC_ID = 1,
	SEMAP_SOCKET_DESTROY_ID = 2
};

enum DISCONNECT_EVENT_FROM
{
	FROM_EVENT_SOCKET_CONTROLLER = 0,
	FROM_FILE_SOCKET_CONTROLLER = 1,
	FROM_MOUSE_SOCKET_CONTROLLER = 2,
};

#endif

