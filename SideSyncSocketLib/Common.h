#pragma once

#define MAX_SEM_COUNT 1
#define _FILE_OFFSET_BITS 64
#define _LARGEFILE64_SOURCE

#define ENABLE_SLOW_FILE_TRANSFER

#define FILE_TRANSFER_SLOWED_DOWN_BY_MS 500
#define MOUSE_KEYBOARD_EVENT_DETECT_INTERVAL 2.0


#define MAX_SEM_COUNT 1

enum FileOperationMode
{
	MODE_READ = 0,
	MODE_WRITE
};

#define PAL_SLEEP_MS(val) Sleep(val)
#define T_SEMAPHORE HANDLE
#define DECLARE_SEMAPHORE(__var) HANDLE __var
#define CREATE_SEMAPHORE(__var) __var = CreateSemaphore(NULL,0, MAX_SEM_COUNT, NULL)
#define WAIT_SEMAPHORE(__var) WaitForSingleObject(__var, 100000L)
#define WAIT_SEMAPHORE_TIMEOUT(__var,_timeout) WaitForSingleObject(__var, _timeout)
#define RELEASE_SEMAPHORE(__var) ReleaseSemaphore(__var,1,NULL)
#define DESTROY_SEMAPHORE(__var) CloseHandle(__var)
#define RESET_SEMAPHORE(__var) CloseHandle(__var); __var = CreateSemaphore(NULL,0, MAX_SEM_COUNT, NULL)

#define PAL_FILE FILE*
#define PAL_BAD_FILE NULL
#define PAL_PTHREAD_COMP(pth1, pth2) &pth1.p == &pth2.p && pth1.x == pth2.x
#define PAL_GET_THREAD_ID GetCurrentThreadId()


#ifdef _DEBUG
    #pragma comment(linker, "/nodefaultlib:msvcrt.lib")
	#define _AFXDLL
#endif

#pragma warning(disable:4098)
#pragma warning(disable:4005)
#pragma warning(disable:4996)

#if !defined(_WIN32_WINNT)
#define _WIN32_WINNT 0x0600
#endif

//#include <afx.h>
//#include <WinSock2.h>
//#include <Ws2tcpip.h>
//#include "..\Extern\pthread\include\pthread.h"
#include <cstring>
#include <stdlib.h>
//#include <crtdbg.h>

#define WM_SIDESYNC_LOG_PRINT_MESSAGE (WM_USER+1)
#define WM_SIDESYNC_LOG_EXIT_MESSAGE (WM_USER+2)

#define WM_SIDESYNC_NOTIFY_SEND_MESSAGE (WM_USER+3)
#define WM_SIDESYNC_NOTIFY_EXIT_MESSAGE (WM_USER+4)

typedef int socklen_t;
#define inet_ntop InetNtop
//#define close closesocket
#define usleep(...) Sleep((__VA_ARGS__)/1000)
#define INET6_ADDRSTRLEN 46
typedef BOOL SOCK_OPT;
#define _YES TRUE

#define DECLARE_MUTEX CRITICAL_SECTION cs
#define CREATE_MUTEX InitializeCriticalSection(&cs)
#define LOCK_MUTEX EnterCriticalSection(&cs)
#define UNLOCK_MUTEX LeaveCriticalSection(&cs)
#define DELETE_MUTEX 

#define DECLARE_MUTEX_V(var) CRITICAL_SECTION var
#define CREATE_MUTEX_V(var) InitializeCriticalSection(&var)
#define LOCK_MUTEX_V(var) EnterCriticalSection(&var)
#define UNLOCK_MUTEX_V(var) LeaveCriticalSection(&var)
#define DELETE_MUTEX_V(var)

enum {
	SHUT_RD = 0,        /* no more receptions */
#define SHUT_RD         SHUT_RD
	SHUT_WR,            /* no more transmissions */
#define SHUT_WR         SHUT_WR
	SHUT_RDWR           /* no more receptions or transmissions */
#define SHUT_RDWR       SHUT_RDWR
};




#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>

#define gettid() syscall(__NR_gettid)  

#define IPTOS_LOWDELAY 0x10
#define SAFE_DELETE(obj) if (obj) {delete obj;obj=NULL;}
#define SAFE_DELETE_ARRAY(obj) if (obj) {delete [] obj;obj=NULL;}

static const int LOG_EVENT = 0;
static const int MEG_REV_EVENT = 1;
static const int TIME_REFRESH_EVENT = 2;

typedef unsigned long long c_uint64;		 /* Unsigned 64 bit value */
typedef  unsigned long int  c_uint32;      /* Unsigned 32 bit value */
typedef  unsigned short     c_uint16;      /* Unsigned 16 bit value */
typedef  unsigned char      c_uint8;       /* Unsigned 8  bit value */
typedef  signed long long	c_int64;		 /* Signed 64 bit value */
typedef  signed long int    c_int32;       /* Signed 32 bit value */
typedef  signed short       c_int16;       /* Signed 16 bit value */
typedef  char				c_int8;        /* Signed 8  bit value */
typedef  unsigned char		c_bool;	 /* bool */
#ifndef c_byte
typedef unsigned char		c_byte;
#endif

void *get_in_addr(struct sockaddr *sa); 
int byte2Int(char* src, int startIdx, bool big_endian);
wchar_t * utf8_to_unicode_n(const char *pUtf8);
char* unicode_to_utf8_n(const wchar_t *pUnicode);
char* wchar_to_char(const wchar_t *pUnicode);

char* copyStr(const char* source, int len = -1);

c_uint64 byte2Long(char* src, int startIdx, bool big_endian) ;

unsigned int PAL_FILE_READ(PAL_FILE _file, char* _buffer, int _nSize);
unsigned int PAL_FILE_WRITE(PAL_FILE _file, char* _buffer, int _nSize);
int PAL_FILE_SEEK(PAL_FILE _file, c_uint64 _seek_pos, int _seek_dir);
c_uint64 PAL_FILE_TELL(PAL_FILE _file);
PAL_FILE PAL_FILE_OPEN(const char* _path, FileOperationMode _mode);
void PAL_FILE_CLOSE(PAL_FILE _file);

c_uint64 mergeTo64(int part1, int part2);
void splitTo32(c_uint64 number, unsigned int* part1, unsigned int* part2);
c_uint64 getCurrTimeMs();



struct CallBackMessage
{ 
	int eventID;
	int arg1;
	long arg2;
	char* message;
};

typedef c_uint64 uint64_t;