#pragma once

#include "Common.h"

enum NOTIFY_DATA_TYPE
{
	CHAR_PTR = 0, BYTE_PTR, INTEGER_PTR, DATA_STR
};

struct DataStr
{
	char* file_name;
	c_uint32 part1;
	c_uint32 part2;
};

#include "ISideSyncSocketEventListener.h"

typedef struct NotifyParam
{
	int event_id;
	int arg1;
	int arg2;
	const void* data;
	int data_len;
	int data_type;
	char* data_extended;

	NotifyParam()
	{
		event_id = -1;
		arg1 = arg2 = data_len = data_type = -1;
		data = 0;
		data_extended = 0;
	};
};

class NotifyFromNative
{
public:
	NotifyFromNative();
	static bool bIsEventListenerSet;
	DWORD _senderThread;
	HANDLE _threadHandle;
	ISideSyncSocketEventListener* mSideSyncEventListener;

	static void* senderThreadMain(void* arg);

	static void setListener(ISideSyncSocketEventListener* pListener);
	
	static void Notify(int eventID, int arg1 = -1, int arg2 = -1, const void* data = NULL, int data_len = 0, int dataType = CHAR_PTR, char* data_extended = NULL);
private:
	void notify(int eventID, int arg1 = -1, int arg2 = -1, const void* data = NULL, int data_len = 0, int dataType = CHAR_PTR, char* data_extended = NULL);
};
