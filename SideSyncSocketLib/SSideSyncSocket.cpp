#include "stdafx.h"
#include "SSideSyncSocket.h"

SSideSyncSocket::SSideSyncSocket()
{
	_sockMgr = NULL;
	_socketMode = SSM_CLIENT;
	_socketEventListener = NULL;
}

SSideSyncSocket::~SSideSyncSocket()
{
}

SideSyncSocketError SSideSyncSocket::Initialize( int port, BOOL isServer )
{
	if (_sockMgr != NULL)
		return ERROR_SOCKET_ALREADY_CREATED;

	WSADATA wsaData;
	int err = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (err != 0)
	{
		LOGD("\r\nWSAStartup() failed, e : %d", err);
		return ERROR_UNKNOWN;
	}

	_socketMode = isServer == TRUE ? SSM_SERVER : SSM_CLIENT;

	_sockMgr = new SocketManager(port, this, this, /*instantID*/port, isServer == TRUE ? true : false);
	//CREATE_SEMAPHORE(_semaConfirmConnect);
	CREATE_SEMAPHORE(_semaDestroy);

	return OK;
}

SideSyncSocketError SSideSyncSocket::Destroy()
{
	if (_sockMgr == NULL)
		return OK;

	_sockMgr->setConfirmState(false);

	CleanUp();

	WSACleanup();

	return OK;
}

SideSyncSocketError SSideSyncSocket::Connect( LPCTSTR ipAddr )
{
	if (_sockMgr == NULL)
	{
		LOGE("Call initialize function first");
		return ERROR_NOT_INITIALIZED;
	}

	if (_sockMgr->isConnected())
	{
		LOGE("connect() - ERROR_SOCKET_ALREADY_CREATED");
		return ERROR_SOCKET_ALREADY_CREATED;
	}

	if (ipAddr == NULL || _tcslen(ipAddr) == 0)
	{
		LOGE("Input IP address first.");
		return ERROR_UNKNOWN;
	}

#ifdef UNICODE
	CStringA ipAddrA(ipAddr);
	char* connectIP = copyStr(ipAddrA);
#else
	char* connectIP = copyStr(ipAddr);
#endif

	LOGD("Connecting with Server IP %s", connectIP);

	int result = OK;

	//_isConnecting = TRUE;

	int err = _sockMgr->createSocket(connectIP);
	delete[] connectIP;

	if (err != OK)
	{
		LOGE("start_server() failed, err : %d", err);		
		return _socketMode == SSM_SERVER ? ERROR_CANNOT_START_SERVER : ERROR_CANNOT_CONNECT_TO_SERVER;
	}

	LOGD("_sockMgr->createSocket() succeeded.");

	return OK;
}

SideSyncSocketError SSideSyncSocket::Disconnect()
{
	if (_sockMgr == NULL)
	{
		LOGE("Call initialize function first");
		return ERROR_NOT_INITIALIZED;
	}

	LOGD("Disconnect Called by Application");

	//_isConnecting = FALSE;

	bool connected = _sockMgr->isConnected();
	_sockMgr->closeSocket();

	LOGD("Close connection");
	return OK;
}

SideSyncSocketError SSideSyncSocket::SendRequest( int deviceId, int sid, byte* msg, int msg_len )
{
	if (_sockMgr == NULL)
	{
		LOGE("Call initialize function first");
		return ERROR_NOT_INITIALIZED;
	}

	if (!_sockMgr->isConnected())
	{
		LOGE("_sockMgr::cancel_file_sending - No client connected");
		return ERROR_NO_CLIENT_CONNECTED;
	}

	char* msgcpy = new char[msg_len];
	memcpy(msgcpy, msg, msg_len);

	SendTask * task = new SendTask();
	task->task_type = SocketData_Request;
	task->setObj(msgcpy, msg_len);
	task->arg1 = sid;

	int _taskID = _sockMgr->getSendLooper()->submitTask(task);

	delete task;
	delete[] msgcpy;

	return OK;
}

SideSyncSocketError SSideSyncSocket::SendResponse( int deviceId, int sid, byte* msg, int msg_len )
{
	if (_sockMgr == NULL)
	{
		LOGE("Call initialize function first");
		return ERROR_NOT_INITIALIZED;
	}

	if (!_sockMgr->isConnected())
	{
		LOGE("_sockMgr::cancel_file_sending - No client connected");
		return ERROR_NO_CLIENT_CONNECTED;
	}

	char* msgcpy = new char[msg_len];
	memcpy(msgcpy, msg, msg_len);

	SendTask * task = new SendTask();
	task->task_type = SocketData_Response;
	task->setObj(msgcpy, msg_len);
	task->arg1 = sid;

	int _taskID = _sockMgr->getSendLooper()->submitTask(task);

	delete task;
	delete[] msgcpy;

	return OK;
}

SideSyncSocketError SSideSyncSocket::SendPacket( byte* data, int data_len )
{
	if (_sockMgr == NULL)
	{
		LOGE("Call initialize function first");
		return ERROR_NOT_INITIALIZED;
	}

	if (!_sockMgr->isConnected())
	{
		LOGE("_sockMgr::cancel_file_sending - No client connected");
		return ERROR_NO_CLIENT_CONNECTED;
	}

	char* datacpy = new char[data_len];
	memcpy(datacpy, data, data_len);

	SendTask * task = new SendTask();
	task->task_type = SocketData_Packet;
	task->setObj(datacpy, data_len);
	task->arg1 = 0;
	task->arg2 = 0;

	int _taskID = _sockMgr->getSendLooper()->submitTask(task);

	return OK;
}


void SSideSyncSocket::SetEventListener( ISideSyncSocketEventListener* listener )
{
	//NotifyFromNative::setListener(listener);
	_socketEventListener = listener;
}

void SSideSyncSocket::CleanUp()
{
	//RELEASE_SEMAPHORE(_semaConfirmConnect);

	if (_sockMgr != NULL)
	{
		Disconnect();

		if (_sockMgr->isReceiveThreadRunning())
		{
			LOGD("<<WAIT DESTROY SEMAPHORE>> - IN");

			WAIT_SEMAPHORE_TIMEOUT(_semaDestroy, 3000L);

			LOGD("<<WAIT DESTROY SEMAPHORE>> - OUT");
		}

		while (_sockMgr->getConfirmState())
		{
			PAL_SLEEP_MS(10);
		}

		SAFE_DELETE(_sockMgr);
	}

	//DESTROY_SEMAPHORE(_semaConfirmConnect);
	DESTROY_SEMAPHORE(_semaDestroy);
}

void SSideSyncSocket::onAcceptListener( int socketid, char* ipaddr )
{
	LOGD("on_accept_listener - socketid = %d ip=%s", socketid, ipaddr);
}

void SSideSyncSocket::onConnectedListener( int socketid, char* ipaddr )
{
	LOGD("on_connected_listener - socketid = %d ip=%s", socketid, ipaddr);

	if (_sockMgr == NULL)
	{
		LOGE("on_connected_listener() - mSocketManager == NULL");
		return;
	}

	_sockMgr->setConnected(true);
	_sockMgr->setConfirmState(true);

	if (_socketEventListener != NULL)
		_socketEventListener->onConnected(_sockMgr->getConnectionType(), (LPCTSTR)ipaddr);
}

void SSideSyncSocket::onConnectionFailedListener( int socketid, char* ipaddr )
{
	LOGE("onConnectionFailedListener() is called");

	if (_sockMgr != NULL && _socketEventListener != NULL)
		_socketEventListener->onConnectFailed(_sockMgr->getConnectionType(), (LPCTSTR)ipaddr);
}

void SSideSyncSocket::onReceiveListener( int socketid, char* ipaddr, char* data, int data_len )
{
	LOGE("onReceiveListener() is called");
}

void SSideSyncSocket::onClientCloseListener( int socketid, char* ipaddr )
{
	LOGE("onClientCloseListener() is called");

	if (_sockMgr != NULL && _socketEventListener != NULL)
		_socketEventListener->onDisconnected(_sockMgr->getConnectionType(), (LPCTSTR)ipaddr);
}

void SSideSyncSocket::onSocketDestroy()
{
	LOGD("onSocketDestroy() is called");

	RELEASE_SEMAPHORE(_semaDestroy);
}
