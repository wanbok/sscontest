#pragma once

#include "SSideSyncSocket.h"
#include "..\MiracastSink\ReciverHdr.h"

#define IDC_HEADER_SIZE		2

static void* g_pIDCParent = NULL;
static IDCcallback_Recv		m_IDCCallback_Recv = NULL;		// Recv 정보 전달

class SSideSyncIDCSocket : public SSideSyncSocket
{
public:
	SSideSyncIDCSocket();
	virtual ~SSideSyncIDCSocket();

	virtual SideSyncSocketError Initialize(int port, BOOL isServer);
	virtual SideSyncSocketError Destroy();

	virtual SideSyncSocketError Connect(LPCTSTR ipAddr);
	virtual SideSyncSocketError Disconnect();

	virtual SideSyncSocketError SendPacket(byte* data, int data_len);

	virtual SSendPacket* CreatePacket(char* data, c_uint64 len);

	//(hansol) 아래 두 함수를 구현해야 receive 동작합니다.
	virtual int			onRequestHeaderSize();
	virtual c_uint32	onRequestPacketSizeFromHeader(int headerSize, char* header);

	//(hansol) 아래 Listener는 Recv 마무리된 Packet 전체를 data 에 넣어 전달하는 함수이므로 이것을 구현해 recv 관련 처리 하세요. (packet 내 header 도 같이 있음)
	virtual void onReceiveListener(int socketid, char* ipaddr, char* data, int data_len);

	void SetParent(void* pParent);
	void SetCallbackRecv(IDCcallback_Recv pIDCCallback);

};
