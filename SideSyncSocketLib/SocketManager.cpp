#include "stdafx.h"

#include "SocketManager.h"
//#include "SocketPacketFactory.h"
#include "ErrorCodes.h"
#include "Settings.h"

#include "Iphlpapi.h"
#pragma comment(lib, "iphlpapi.lib")

#define LOG_TAG "SocketManager"
#define LOCAL_IP_ADDR "127.0.0.1"

static int MAX_PACKET_SIZE = 1459;

/**
 *Construct function of SocketManager to create an instance
 *@param port port number of socket
 *@param owner the interface pointer to receive event from SocketManager
 *@param instanceID unique identity of this instance
 */
SocketManager::SocketManager(int port, ISocketManagerListener* owner, IPacketMaker* ownerPacketMaker, int instanceID, bool isServer)
{

	__connectionType = -1;
	__pOwner = owner;
	__pOwnerPacketMaker = ownerPacketMaker;
	__port = port;

	__isSocketConnected = false;
	__isSocketCreated = false;
	__isConfirming = false;
	__isReceiveThreadRunning = false;
	__isConnectThreadRunning = false;
	__isDeviceConnected = false;

	__pSendThreadLooper = NULL;

	__instantID = instanceID;

	//If this instance work as FileSocket and connection type is USB,
	//double the packet size
	//if (__instantID > -1 && __connectionType == USB_ADB)
	//{
	//	MAX_PACKET_SIZE = 2 * MAX_PACKET_SIZE;
	//}

	//Set default value for local device IP Address
	memset(__serverIp, 0, (sizeof(char) * INET6_ADDRSTRLEN));

	__needSecurity = true;
	__authenticationInfo = NULL;
	__authenticationInfoLength = 0;

	__connectedViaWifi = false;

	__isServer = isServer;

	/*if (__instantID ==-1)
	 {
	 void* address = static_cast<void*>(this);
	 Settings::getInstance()->pSocketManagerAddr = (uintptr_t)address;
	 LOGD("<<<Address>>> %ld", Settings::getInstance()->pSocketManagerAddr);
	 }*/
	LOGD("Connect as %s", __isServer?"Server":"Client");
}

/**
 * Destructor function to release allocated memory
 */
SocketManager::~SocketManager()
{
	//__pOwner = NULL;

	PAL_SLEEP_MS(300);

	//Destroy send looper
	if (__pSendThreadLooper)
	{
		delete __pSendThreadLooper;
		__pSendThreadLooper = NULL;
	}

	//Destroy authentication information
	if (__authenticationInfo)
	{
		free(__authenticationInfo);
		__authenticationInfo = NULL;
		__authenticationInfoLength = 0;
	}
}

/**
 * create an internal socket to start listening (Server) for client connection or connecting (Client) to remote server.
 * @param ipaddr the IP Address used only when the Client connects to Server
 * @return Error code or 0 if success
 */
int SocketManager::createSocket(char* ipaddr)
{
	LOGD("startListen()[%d] - IN", __instantID);

	int err = 0;

	if (__isSocketCreated == false)
	{
		LOGD("SocketManager()[%d]: startListen() IN", __instantID);

		__isSocketCreated = true;
		if (ipaddr) strcpy(__serverIp, ipaddr);

		pthread_create(&__pthreadConnect, NULL, connectThreadMain, (void*) this);

		LOGD("SocketManager()[%d]: startListen() OUT", __instantID);
	}
	else
	{
		LOGE("SocketManager()[%d]: is already created", __instantID);
		LOGD("startListen()[%d] - OUT", __instantID);
		return ERROR_SOCKET_ALREADY_CREATED;
	}

	LOGD("startListen()[%d] - OUT", __instantID);
	return OK;
}

/**
 * Thread function to process connect procedures
 */
void* SocketManager::connectThreadMain(void *arg)
{

	LOGD("connect_thread_main() - IN");

	SocketManager *h = reinterpret_cast<SocketManager *>(arg);
	int ret = h->connectThreadFunc();

	LOGD("connect_thread_main() - OUT ret = %d", ret);
	return 0;
}

/**
 * Internal function to process connect thread
 * @return Error code or 0 if success
 */
int SocketManager::connectThreadFunc()
{

	LOGD("connect_thread_loop()[%d] - IN", __instantID);

	__sockFd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (__sockFd < 0)
	{
		LOGE("ERROR opening socket - error code:%d", __sockFd);
		LOGD("connect_thread_loop()[%d] - OUT", __instantID);
		return ERROR_SOCKET_CANT_OPEN;
	}
	int yes = 1;

	if (setsockopt(__sockFd, SOL_SOCKET, (SO_REUSEADDR), (char*)&yes, sizeof(int)) == -1)
	{
		LOGE("setsockopt SO_REUSEADDR");
		LOGD("connect_thread_loop() - OUT");
		return ERROR_SOCKET_CANT_SET_OPT;
	}

	int len, trysize, gotsize;
	len = sizeof(int);
	getsockopt(__sockFd, SOL_SOCKET, SO_SNDBUF, (char*) &gotsize, &len);

	LOGE("Got size: %d",gotsize);

	trysize = 512*1024;
	int err = setsockopt(__sockFd, SOL_SOCKET, SO_SNDBUF, (char*) &trysize, len);
	if (err<0)
	{
		LOGE("can't setsockopt SO_SNDBUF");
		LOGD("connect_thread_loop() - OUT");
	}

	err = setsockopt(__sockFd, SOL_SOCKET, SO_RCVBUF, (char*) &trysize, len);
	if (err<0)
	{
		LOGE("can't setsockopt SO_SNDBUF");
		LOGD("connect_thread_loop() - OUT");
	}

	__isConnectThreadRunning = true;

	__pSendThreadLooper = new SendThreadLooper(__instantID);
	__pSendThreadLooper->SetPacketMaker(__pOwnerPacketMaker); 

	int result = OK;
	if (__isServer == true)

		result = connectAsServer();
	else
		result = connectAsClient();

	__isConnectThreadRunning = false;
	__isSocketCreated = false;

	LOGD("connect_thread_loop()[%d] - OUT", __instantID);

	return result;
}

bool SocketManager::isReceiveThreadRunning()
{
	return __isReceiveThreadRunning;
}

/**
 * Thread function to receive data from socket
 */
void* SocketManager::receiveThreadMain(void *arg)
{

	LOGD("receive_thread_main() - IN");

	SocketManager *h = reinterpret_cast<SocketManager *>(arg);
	h->receiveThreadLoop();

	LOGD("receive_thread_main() - OUT");
	return 0;
}

/**
 * Internal function to receive data from socket
 */
void SocketManager::receiveThreadLoop()
{

	LOGD("receive_thread_loop()[%d] - IN", __instantID);
	__isReceiveThreadRunning = true;
	char* buf = new char[MAX_PACKET_SIZE];
	int res = 0;

	__socketBuffStream.clear();
	/* Receive data loop */
	while (__isReceiveThreadRunning)
	{
		res = receiveData(buf);
		if (res < 0) break;

		if (PAL_PTHREAD_COMP(pthread_self(),__pthreadReceive) == false)
		{
			LOGE("receiveThreadLoop() - Thread[%d] is killed", pthread_self());
			break;
		}
	}
	SAFE_DELETE_ARRAY(buf);

	LOGD("receive_thread_loop()[%d] - OUT", __instantID);

	if (__pOwner) __pOwner->onSocketDestroy();

	__isReceiveThreadRunning = false;
	LOGD("receive_thread_loop()[%d] - OUT", __instantID);
}

/**
 * When works as Server, this function is called to process connect procedures
 * @return Error code or 0 if success
 */
int SocketManager::connectAsServer(void)
{
	LOGD("connect_as_server()[%d] - IN", __instantID);

	//Set default value for myIP address
	if (strlen(__serverIp) == 0) sprintf(__serverIp, "%s", LOCAL_IP_ADDR);

	LOGD("Server_IP = %s", __serverIp);

	/* Initialize socket structure */
	memset((char *) &__servAddr, 0, sizeof(__servAddr));
	__servAddr.sin_family = PF_INET;
	__servAddr.sin_addr.s_addr = INADDR_ANY;
	__servAddr.sin_port = htons(__port);

	/* Now bind the host address using bind() call.*/
	int ret = bind(__sockFd, (struct sockaddr *) &__servAddr, sizeof(__servAddr));
	if (ret < 0) //If binding fail
	{
		perror("ERROR on binding");
		LOGE( "SocketManager()[%d]: ERROR on binding - Code: %d", __instantID, ret);
		LOGD("connect_as_server()[%d] - OUT", __instantID);

		return ERROR_CANNOT_BIND_SOCKET;
	}

	//Listen to new connection from client
	if (listen(__sockFd, BACKLOG) == -1)
	{
		perror("listen");
		LOGD("connect_as_server()[%d] - OUT", __instantID);

		return ERROR_CANNOT_LISTEN_SOCKET;
	}
	else
	{
		LOGD("connect_as_server()[%d] success", __instantID);
	}

	sin_size = sizeof __theirAddr;

	//Looper to accept connection from clients
	while (__isConnectThreadRunning)
	{
		if (PAL_PTHREAD_COMP(pthread_self(),__pthreadConnect) == false)
		{
			LOGE("connectAsServer() - Thread[%d] is killed", pthread_self());
			return -1;
		}

		LOGD("SocketManager()[%d]: waiting for connections...", __instantID);

		//Accept new connection from client
		__clientFd = accept(__sockFd, (struct sockaddr *) &__theirAddr, &sin_size);
		if (__clientFd == -1) //if has error
		{
			perror("accept");
			LOGE("SocketManager()[%d]: pc_client_fd == -1", __instantID);
			//
			LOGD("connect_as_server()[%d] - OUT", __instantID);
			return ERROR_CANNOT_ACCEPT_CLIENT_SOCKET;
		}

		/* Obtain the client's IP Address (remote IP address) */
		inet_ntop(__theirAddr.ss_family,
				get_in_addr((struct sockaddr *) &__theirAddr), (PTSTR)__remoteIp,
				sizeof(__remoteIp));

		LOGD("client IP: %s", __remoteIp);
		/*-----------------------------------------------------*/

		determineConntectionType();

		__isSocketConnected = true;

		// TODO: check read thread is already made
		//Start a thread to receive data from socket
		if(__pOwner != NULL && __pOwner->onRequestHeaderSize() > 0)
		{
			pthread_create(&__pthreadReceive, NULL, receiveThreadMain, (void*) this);
		}

		LOGD("SocketManager()[%d]: Send onAccept event ", __instantID);

		//Stop existed Send Thread Looper first
		if (__pSendThreadLooper->isRunning())
		{
			LOGD( "SocketManager()[%d]: Destroy existed thread looper ", __instantID);
			__pSendThreadLooper->stop();
		}

		//Start Thread Looper
		LOGD("SocketManager()[%d]: Create new thread looper ", __instantID);

		// If connected via ADB, encryption is not needed.
		if (__connectionType == 0) // USB_ADB
		{
			LOGD("Connected via USB");
			__connectedViaWifi = false;

			__needSecurity = false;
			__pSendThreadLooper->start(__clientFd, false);

		}
		// If connected via Wi-Fi, encryption is needed.
		else
		{

			__connectedViaWifi = true;
			__needSecurity = true;

			setAuthenticationInfo(__remoteIp);
			__pSendThreadLooper->start(__clientFd, true);
		}

		//Send accept connection event to listener (EventSocketController)
		LOGD("SocketManager()[%d]: Send accept event ", __instantID);
		if (__pOwner)
			__pOwner->onAcceptListener(__instantID, __remoteIp);

	}

	LOGD("connect_as_server()[%d] - OUT", __instantID);
	return OK;
}

/**
 * When works as Client, this function is called to process connect procedures
 */
int SocketManager::connectAsClient(void)
{

	LOGD("connect_as_client()[%d] - IN", __instantID);

	//Set default value for myIP
	if (strlen(__serverIp) == 0)
	{
		return ERROR_CANNOT_CONNECT_TO_SERVER;
	}

	/* Initialize socket structure */
	memset((char *) &__servAddr, 0, sizeof(__servAddr));

	__servAddr.sin_family = AF_INET;
	__servAddr.sin_addr.s_addr = inet_addr(__serverIp);
	__servAddr.sin_port = htons(__port);

	//Connect to remote Server thru serverIP
	__clientFd = connect(__sockFd, (struct sockaddr *) &__servAddr, sizeof(__servAddr));

	if (__clientFd == -1) //error
	{

		LOGE("SocketManager()[%d]: __clientFd == -1 serverIP = %s", __instantID, __serverIp);
		LOGD("connect_as_client()[%d] - OUT", __instantID);

		return ERROR_CANNOT_CONNECT_TO_SERVER;
	}
	else
	{
		LOGD("connect_as_client()[%d] success",__instantID);
	}

	__clientFd = __sockFd;

	/* Copy server IP Address */
	strcpy(__remoteIp, __serverIp);

	determineConntectionType();

	__isSocketConnected = true;

	//Start a thread to receive data from socket
	pthread_create(&__pthreadReceive, NULL, receiveThreadMain, (void*) this);

	//Start Thread Looper
	if (__pSendThreadLooper->isRunning())
	{
		LOGD("SocketManager[%d]: Destroy existed thread looper ", __instantID);
		__pSendThreadLooper->stop();
	}

	LOGD("SocketManager[%d]: Create new thread looper ", __instantID);

	// If connected via ADB, encryption is not needed.
	if (__connectionType == 0) // USB_ADB
	{
		LOGD("Connected via USB");
		__connectedViaWifi = false;

		__needSecurity = false;
		__pSendThreadLooper->start(__clientFd, false);

	}
	// If connected via Wi-Fi, encryption is needed.
	else
	{
		// Get (Client) Local Address of Connected Socket
		//struct sockaddr_in myaddr;
		//int myaddr_len = sizeof(myaddr);
		//getsockname(__clientFd, (struct sockaddr *) &myaddr, &myaddr_len);

		//char* local_ip = inet_ntoa(myaddr.sin_addr);
		LOGD("local address : %s\n", __accptedInterfaceIP);

		__connectedViaWifi = true;
		__needSecurity = true;
		setAuthenticationInfo(__accptedInterfaceIP);

		__pSendThreadLooper->start(__clientFd, true);

	}

	LOGD("SocketManager[%d]: Send onConnnect event ", __instantID);
	if (__pOwner)
		__pOwner->onConnectedListener(__instantID, __remoteIp);

	LOGD("connect_as_client()[%d] - OUT", __instantID);
	return OK;
}

/**
 * Internal function to receive data from socket. When receive -1 from socket, the connection is lost
 */
int SocketManager::receiveData(char* buf)
{
	if(__pOwner == NULL)
	{
		return ERROR_SOCKET_NOT_SET_OWNER;
	}

	c_uint32 numbytes = 0;
	c_uint32 read_max_bytes = MAX_PACKET_SIZE;

	numbytes = recv(__clientFd, buf, read_max_bytes, 0);

	//Receive data from socket. This is block function so we call shutdown socket to break this call
	if (numbytes == -1) //If error
	{
		LOGD("recv() == -1. receive_data()[%d] - OUT", __instantID);
		if (__isSocketConnected)
		{
			__isSocketConnected = false;

			if (__pOwner && __isDeviceConnected)
				__pOwner->onClientCloseListener(__instantID, __remoteIp);

			__isDeviceConnected = false;

			return ERROR_RECV_FAILED;
		}
		return OK;
	}
	else if (numbytes > 0) //If OK
	{
		__isDeviceConnected = true;
		__socketBuffStream.write(buf, numbytes); //Write data to buffer

		char* _readBuff;

		int headerSize = __pOwner->onRequestHeaderSize();
		//Parse the received data if any complete packet is received
		while (__socketBuffStream.getPendingDataLen() >= headerSize)
		{
			_readBuff = new char[headerSize]();
			c_uint32 _readnum = __socketBuffStream.read(_readBuff, headerSize, true);

			//If read data from buffer is less than header size
			if (_readnum < headerSize)
			{
				SAFE_DELETE_ARRAY(_readBuff);
				break;
			}

			//Get PacketSize From Header
			c_uint32 pack_length = __pOwner->onRequestPacketSizeFromHeader(headerSize, _readBuff);

			//If packet length is bigger than available data, the packet is not received completely
			if (pack_length > __socketBuffStream.getPendingDataLen())
			{
				break;
			}

			//Read full packet data from buffer
			_readBuff = new char[pack_length];
			_readnum = __socketBuffStream.read(_readBuff, pack_length);
			if (_readnum > 0 && __pOwner)
				__pOwner->onReceiveListener(__instantID, __remoteIp, _readBuff, _readnum);
			if (__socketBuffStream.getPendingDataLen() == 0) __socketBuffStream.clear(); // Clear buffer stream

		}; //All packets are parsed

		_readBuff = NULL;

		return OK;
	}
	else //client closed socket
	{
		LOGD("Client is disconnected [%d]", __instantID);

		//Stop send looper
		if (__pSendThreadLooper)
		{
			LOGD("Stop SendThreadLooper ", __instantID);

			__pSendThreadLooper->stop();
		}

		//Send event to client
		if (__isDeviceConnected == true && __pOwner)
			__pOwner->onClientCloseListener(__instantID, __remoteIp);

		__isSocketConnected = false;
		__isDeviceConnected = false;

		__clientFd = -1;

		__socketBuffStream.clear();

		//Release IP add
		memset(__remoteIp, 0x0, sizeof(__remoteIp));

		LOGD("receive_data() - [%d] - OUT", __instantID);
		return ERROR_SOCKET_CLOSED;
	}
}

/**
 * Close the connection of socket. After calling this, createSocket need to be called again to open the socket
 * @return 0: if success otherwise fail
 */
int SocketManager::closeSocket()
{

	LOGD("[%d]stopListen() - IN", __instantID);

	__isSocketCreated = false;

	//If socket is connected, shutdown it now to release the receive function call
	if (__isSocketConnected)
	{

		LOGD("[%d]stopListen() - shutdown socket", __instantID);
		shutdown(__clientFd, SHUT_RDWR);
		closesocket(__clientFd);
	}

	//Shutdown the local socket
	shutdown(__sockFd, SHUT_RDWR);
	closesocket(__sockFd);

	//Stop Send Thread Looper
	if (__pSendThreadLooper)
	{
		__pSendThreadLooper->stop();
		delete (__pSendThreadLooper);
		__pSendThreadLooper = NULL;
	}

	//Stop data receive thread
	if (__isReceiveThreadRunning)
	{
		__isReceiveThreadRunning = false;
	}

	//Stop waiting connection of server
	if (__isServer && __isConnectThreadRunning)
	{
		__isConnectThreadRunning = false;
	}

	__isDeviceConnected = false;

	LOGD("[%d]stopListen() - OUT", __instantID);
	return OK;
}

/**
 * Get connected socket
 * @return socketid
 */
int SocketManager::getClientSocket()
{
	return __clientFd;
}

/**
 * Check if socket is connected
 * @return true: connected \n false: disconnected
 */bool SocketManager::isSocketConnected()
{

	if (__isSocketConnected || __isConfirming)
		return true;
	else
		return false;

	// return is_connected;
}

/**
 * Get SendLooper
 * @return NULL if socket is disconnected otherwise return internal SendThreadLooper
 */
SendThreadLooper* SocketManager::getSendLooper()
{
	if (__isSocketConnected || __isConfirming)
		return __pSendThreadLooper;
	else
		return NULL;
}

/**
 * Check if the socket manager is server
 * @return true if is server otherwise false
 */bool SocketManager::isServer()
{
	return __isServer;
}

/**
 * Set connect status
 * @param state true: connected, false: disconnected
 */
void SocketManager::setConnected(bool state)
{
	__isSocketConnected = state;
}

/**
 * Check if socket is connected
 * @return true: connected, false: disconnected
 */
bool SocketManager::isConnected()
{
	return __isSocketConnected;
	//return __isDeviceConnected;
}

/**
 * Get connection type of socket
 * @return WIFI(1) or USB(0)
 */
int SocketManager::getConnectionType()
{
	return __connectionType;
}

/**
 * Get connected device IP address
 * @return IP Address
 */
char* SocketManager::getConnectedIpAddr()
{
	return __remoteIp;
}

/**
 * Set confirm state when client is waiting for confirm packet (USB mode) or security key exchange (WiFi mode).
 * @param state true or false
 */
void SocketManager::setConfirmState(bool state)
{

	__isConfirming = state;
}

bool SocketManager::getConfirmState()
{

	return __isConfirming;
}
/**
 * Check if the socket need security to encrypt/decrypte data transfer throught WIFI connection
 * @return true: need security, false: no need security
 */bool SocketManager::needSecurity()
{
	return __needSecurity;
}

/**
 * Get security authentication info which is partial MAC Address
 * @param length the length of authentication info
 * @return authentication info
 */
char* SocketManager::getAuthenticationInfo(int& length)
{
	length = __authenticationInfoLength;
	return __authenticationInfo;
}

/**
 * Check if socket is connected via WiFi
 * @return true: if connect via WiFi, false: not WiFi
 */bool SocketManager::isConnectedViaWiFi()
{
	return __connectedViaWifi;
}

/**
 * Set authentication information
 * @param ipAddress is used to get the MAC address which is employed as aunthentication information
 * @return true: if success, false: if fail
 */bool SocketManager::setAuthenticationInfo(char* ipAddress)
{
	// Use parts of MAC address as password for authentication and key exchange.
	if (!isConnectedViaWiFi())
	{
		// This is only for testing secuirty module w/ USB connection. In reality, security will not be used on USB connections.
		__authenticationInfo = (char*) malloc(sizeof(char) * (strlen(ipAddress) + 1));
		strcpy(__authenticationInfo, ipAddress);
		__authenticationInfoLength = strlen(ipAddress);

		return true;
	}

	char* macAddress = getClientMacAddress(ipAddress);
	LOGE("macAddress: %s", macAddress);
	if (macAddress != NULL)
	{
		__authenticationInfo = (char*) malloc(sizeof(char) * 6);

		__authenticationInfo[0] = macAddress[7];
		__authenticationInfo[1] = macAddress[3];
		__authenticationInfo[2] = macAddress[10];
		__authenticationInfo[3] = macAddress[16];
		__authenticationInfo[4] = macAddress[6];
		__authenticationInfo[5] = macAddress[15];

		__authenticationInfoLength = 6;

		free(macAddress);
		return true;
	}
	else
		return false;
}

/**
 * Get remote device MAC address
 * @param ipAddress is used to get the MAC address
 * @return return MAC address
 */
char* SocketManager::getClientMacAddress(char* ipAddress)
{
	bool TEST = false;
	if (TEST)
	{
		char* buff = (char*) malloc(sizeof(char) * 20);
		memcpy(buff, "cc:f9:e8:07:af:b4", 20);
		return buff;
	}

	IP_ADAPTER_INFO AdapterInfo[16]; // Allocate information for up to 16 NICs
	DWORD dwBufLen = sizeof(AdapterInfo);// Save memory size of buffer

	DWORD dwStatus = GetAdaptersInfo(AdapterInfo, &dwBufLen);

	PIP_ADAPTER_INFO pAdapterInfo = AdapterInfo;// Contains pointer to current adapter info

	do
	{
		PIP_ADDR_STRING pIpAddr = &(pAdapterInfo->IpAddressList);
		while (pIpAddr)
		{
			LOGD("client IP: %s", pIpAddr->IpAddress.String);
			if ( strcmp(pIpAddr->IpAddress.String, ipAddress)==0)
			{
				char* buff = (char*) malloc (sizeof(char) * 20);
				memset(buff, 0, sizeof(char) * 20);

				sprintf(buff, "%02X:%02X:%02X:%02X:%02X:%02X",
						pAdapterInfo->Address[0],
						pAdapterInfo->Address[1],
						pAdapterInfo->Address[2],
						pAdapterInfo->Address[3],
						pAdapterInfo->Address[4],
						pAdapterInfo->Address[5]);
				LOGD("client MAC: %s", buff);
				return buff;
			}
			pIpAddr = pIpAddr -> Next;
		}
		pAdapterInfo = pAdapterInfo->Next; // Progress through linked list
	}
	while(pAdapterInfo); // Terminate if last adapter

	return NULL;
}

void SocketManager::getInterfaceFromIP(char* acceptedip, char* interfaceName)
{
	IP_ADAPTER_INFO AdapterInfo[16]; // Allocate information for up to 16 NICs
	DWORD dwBufLen = sizeof(AdapterInfo);// Save memory size of buffer

	DWORD dwStatus = GetAdaptersInfo(AdapterInfo, &dwBufLen);

	PIP_ADAPTER_INFO pAdapterInfo = AdapterInfo;// Contains pointer to current adapter info

	while(pAdapterInfo)
	{
		if ( strcmp(pAdapterInfo->IpAddressList.IpAddress.String, acceptedip) == 0)
		{
			switch (pAdapterInfo->Type)
			{

			case MIB_IF_TYPE_ETHERNET:
				sprintf(interfaceName, "rndis");
				return;
			case IF_TYPE_IEEE80211:
				sprintf(interfaceName, "wlan");
				return;
				break;
			default:
				break;
			}
		}
		pAdapterInfo = pAdapterInfo->Next; // Progress through linked list
	}
}

void SocketManager::determineConntectionType()
{
	if ((__remoteIp != NULL) && (strcmp(__remoteIp, LOCAL_IP_ADDR) != 0))
	{
		/*-----------------own ip----------------------*/
		struct sockaddr_in ___localAddress;
		socklen_t ___addressLength = sizeof(___localAddress);
		getsockname(__clientFd, (struct sockaddr*) &___localAddress, &___addressLength);

		std::string interfaceName;

		memset(__accptedInterfaceIP, 0, INET6_ADDRSTRLEN);
		strcpy(__accptedInterfaceIP, inet_ntoa(___localAddress.sin_addr));
		LOGD("Accepted Interface IP %s", __accptedInterfaceIP);

		char szInterface[100];
		memset(szInterface, 0, 100);
		getInterfaceFromIP(__accptedInterfaceIP, szInterface);
		if (szInterface) interfaceName.assign(szInterface);

		if (interfaceName.find("rndis") != std::string::npos)
			__connectionType = 0; //USB_ADB;
		else
			__connectionType = 1; //WIFI;

		LOGD(
			"Accurate Conn type = %d, Interface = %s, ip = %s", __connectionType, interfaceName.c_str(), __accptedInterfaceIP);

		/*-----------------own ip----------------------*/
	}
	else
	{
		__connectionType = 0; // USB_ADB
	}
}
