#include "stdafx.h"
#include "SSideSyncUibcSocket.h"

#define UIBC_INPUT_TYPE_MOUSE_DOWN	0
#define UIBC_INPUT_TYPE_MOUSE_UP	1
#define UIBC_INPUT_TYPE_MOUSE_MOVE	2
#define UIBC_INPUT_TYPE_KEY_DOWN	3
#define UIBC_INPUT_TYPE_KEY_UP		4
#define UIBC_INPUT_TYPE_ZOOM		5
#define UIBC_INPUT_TYPE_VSCROLL		6
#define UIBC_INPUT_TYPE_HSCROLL		7
#define UIBC_INPUT_TYPE_ROTATE		8

SSideSyncUibcSocket::SSideSyncUibcSocket()
{
}

SSideSyncUibcSocket::~SSideSyncUibcSocket()
{
}

SideSyncSocketError SSideSyncUibcSocket::Initialize( int port, BOOL isServer )
{
	return SSideSyncSocket::Initialize(port, isServer);
}

SideSyncSocketError SSideSyncUibcSocket::Destroy()
{
	return SSideSyncSocket::Destroy();
}

SideSyncSocketError SSideSyncUibcSocket::Connect( LPCTSTR ipAddr )
{
	return SSideSyncSocket::Connect(ipAddr);
}

SideSyncSocketError SSideSyncUibcSocket::Disconnect()
{
	return SSideSyncSocket::Disconnect();
}

SideSyncSocketError SSideSyncUibcSocket::SendMouseDownEvent( int x, int y )
{
	return SendMouseEvent(UIBC_INPUT_TYPE_MOUSE_DOWN, x, y);
}

SideSyncSocketError SSideSyncUibcSocket::SendMouseUpEvent( int x, int y )
{
	return SendMouseEvent(UIBC_INPUT_TYPE_MOUSE_UP, x, y);
}

SideSyncSocketError SSideSyncUibcSocket::SendMouseMoveEvent( int x, int y )
{
	return SendMouseEvent(UIBC_INPUT_TYPE_MOUSE_MOVE, x, y);
}

SideSyncSocketError SSideSyncUibcSocket::SendDeviceKeyDownEvent( DEVICE_KEY_TYPE type )
{
	return SendDeviceKeyEvent(UIBC_INPUT_TYPE_KEY_DOWN, type);
}

SideSyncSocketError SSideSyncUibcSocket::SendDeviceKeyUpEvent( DEVICE_KEY_TYPE type )
{
	return SendDeviceKeyEvent(UIBC_INPUT_TYPE_KEY_UP, type);
}

SideSyncSocketError SSideSyncUibcSocket::SendMouseEvent( int inputType, int x, int y )
{
	int packetLength = 0;
	LPBYTE packet = CreateMouseEventPacket(inputType, x, y, packetLength);
	return SendPacket(packet, packetLength);
}

SideSyncSocketError SSideSyncUibcSocket::SendDeviceKeyEvent( int inputType, DEVICE_KEY_TYPE keyType )
{
	int packetLength = 0;
	UCHAR key1 = 0;
	UCHAR key2 = 0;
	GetDeviceKeyCode(keyType, key1, key2);
	LPBYTE packet = CreateKeyEventPacket(inputType, key1, key2, packetLength);
	return SendPacket(packet, packetLength);
}

SideSyncSocketError SSideSyncUibcSocket::SendPacket( byte* data, int data_len )
{
	return SSideSyncSocket::SendPacket(data, data_len);
}

LPBYTE SSideSyncUibcSocket::CreateMouseEventPacket( int inputType, int x, int y, int& packetLength )
{
	int bodyLength = 6;

	packetLength = 13;

	LPBYTE data = new BYTE[packetLength];

	FillUibcHeader(data, packetLength);
	FillUibcGenericInputBodyHeader(data, inputType, bodyLength);

	// The details of the user inputs
	data[7] = 0x01;    // Number of pointers (N)
	data[8] = 0x00;    // Pointer ID
	data[9] = x >> 8;  // X-coordinate
	data[10] = x;
	data[11] = y >> 8; // Y-coordinate
	data[12] = y;

	return data;
}

LPBYTE SSideSyncUibcSocket::CreateKeyEventPacket( int inputType, UCHAR key1, UCHAR key2, int& packetLength )
{
	int bodyLength = 5;

	packetLength = 12;

	LPBYTE data = new BYTE[packetLength];

	FillUibcHeader(data, packetLength);
	FillUibcGenericInputBodyHeader(data, inputType, bodyLength);

	// The details of the user inputs
	data[7] = 0x00;  // Reserved
	data[8] = key1;
	data[9] = key2;
	data[10] = 0x00; // default set null
	data[11] = 0x00;				

	return data;
}

void SSideSyncUibcSocket::DeletePacket( LPBYTE packet )
{
	delete[] packet;
}

void SSideSyncUibcSocket::FillUibcHeader( LPBYTE data, int packetLength )
{
	// Fill UIBC Header
	data[0] = 0x00;              // Version + T + Reserved
	data[1] = 0x00;              // Reserved + Input Category
	data[2] = packetLength >> 8; // Length
	data[3] = packetLength;
}

void SSideSyncUibcSocket::FillUibcGenericInputBodyHeader( LPBYTE data, int inputType, int bodyLength )
{
	// Fill UIBC Generic Input Body Header
	data[4] = inputType;       // Generic Input Type
	data[5] = bodyLength >> 8; // Fields Length
	data[6] = bodyLength;
}

void SSideSyncUibcSocket::GetDeviceKeyCode( DEVICE_KEY_TYPE type, UCHAR& key1, UCHAR& key2 )
{
	switch (type)
	{
	case DKT_MENU:
		key1 = 0xFF;
		key2 = 0x19;
		break;
	case DKT_HOME:
		key1 = 0xFF;
		key2 = 0x1A;
		break;
	case DKT_BACK:
		key1 = 0xFF;
		key2 = 0x1B;
		break;
	default:
		ASSERT(FALSE);
		break;
	}
}
